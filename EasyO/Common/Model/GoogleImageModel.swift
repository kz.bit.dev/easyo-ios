//
//  GoogleImageModel.swift
//  EasyO
//
//  Created by Аскар on 5/30/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct GoogleImageModel: Codable {
    let mediaLink: String?
}
