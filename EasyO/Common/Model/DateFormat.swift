//
//  DateFormat.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

public enum DateFormat {
    case ddMMyyyy(separator: String)
    case yyyyMMdd(separator: String)
    
    var string: String {
        switch self {
        case .ddMMyyyy(let seperator):
            return ["dd", "MM", "yyyy"].joined(separator: seperator)
        case .yyyyMMdd(let separator):
            return ["yyyy", "MM", "dd"].joined(separator: separator)
        }
    }
}
