//
//  LocalStore.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

class LocalStore {
    
    enum Key: String {
        case token
        case currentUser
        case onboardingShowed
        
        var service: String {
            switch self {
            case .token:
                return "DSFLDSFDSF<DKSFDSLFDS@£$@$"
            case .currentUser:
                return "@$@$@£CXK£@R£@L$XLC@£@L$£$"
            case .onboardingShowed:
                return "Q@£XE£@R£@$X£@L$X@£X$£@X$@"
            }
        }
        
        var account: String {
            switch self {
            case .token:
                return "sjdfnskfjwiekdok23o9i4o32j43i24"
            case .currentUser:
                return "sdlkmewkdm2k3mk4n3k4m2k3n423klm"
            case .onboardingShowed:
                return "sakejn24j32n4i32j4o3jk4n324k234"
            }
        }
    }
    
    func getValue<T: LosslessStringConvertible>(forKey key: Key) -> T? {
        switch key {
        case .token, .currentUser:
            let keychainPasswordItem = KeychainPasswordItem(service: key.service, account: key.account)
            return try? T(keychainPasswordItem.readPassword())
        case .onboardingShowed:
            if let t = UserDefaults.standard.value(forKey: key.rawValue) as? T {
                return t
            }
            return nil
        }
    }
    
    func set<T: CustomStringConvertible>(value: T?, forKey key: Key) {
        switch key {
        case .token, .currentUser:
            let keychainPasswordItem = KeychainPasswordItem(service: key.service, account: key.account)
            if let description = value?.description {
                try? keychainPasswordItem.savePassword(description)
            } else {
                try? keychainPasswordItem.deleteItem()
            }
        case .onboardingShowed:
            UserDefaults.standard.set(value, forKey: key.rawValue)
        }
    }
    
    func removeValue(forKey key: Key) {
        switch key {
        case .token, .currentUser:
            let keychainPasswordItem = KeychainPasswordItem(service: key.service, account: key.account)
            try! keychainPasswordItem.deleteItem()
        case .onboardingShowed:
            UserDefaults.standard.set(nil, forKey: key.rawValue)
        }
    }
    
    func removeAllValues() {
        removeValue(forKey: .token)
        removeValue(forKey: .currentUser)
    }
}
