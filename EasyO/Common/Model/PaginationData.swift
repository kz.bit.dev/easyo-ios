//
//  PaginationData.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct PaginationData<T: Codable>: Codable {
    let data: [T]
    let total: Int
}
