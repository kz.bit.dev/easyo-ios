//
//  ObjectId.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

struct ObjectId: Codable, Hashable {
    
    let id: Int
    
    func getParameters() -> [String: Any] {
        return ["id": id]
    }
}
