//
//  AppColor.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit.UIColor

enum AppColor {
    case darkBlue
    case blueSeparator
    case lightGray
    case gray
    case darkGray
    case lightBlue
    case brown
    case whitish
    case grayClick
    case blueSystemButton
    case darkestBlue
    case systemBlue
    case green
    case skyBlue
    
    static var clouds = UIColor(0xecf0f1)
    
    var uiColor: UIColor {
        switch self {
        case .darkBlue: return UIColor(rgb: 14, 29, 63)
        case .blueSeparator: return UIColor(rgb: 76, 75, 99)
        case .lightGray: return UIColor(rgb: 230, 230, 230)
        case .gray: return UIColor(rgb: 163, 163, 163)
        case .darkGray: return UIColor(rgb: 91, 87, 83)
        case .lightBlue: return UIColor(rgb: 239, 243, 250)
        case .brown: return UIColor(rgb: 58, 58, 58)
        case .whitish: return UIColor(rgb: 239, 239, 244)
        case .grayClick: return UIColor(rgb: 217, 217, 217)
        case .blueSystemButton: return UIColor(hexString: "#19639A")
        case .darkestBlue: return UIColor(rgb: 30, 30, 40)
        case .systemBlue: return UIColor(rgb: 0, 122, 255)
        case .green: return UIColor(rgb: 66, 149, 2)
        case .skyBlue: return UIColor(hexString: "#E4F0FA")
        }
    }
    
    var cgColor: CGColor { return uiColor.cgColor }
}

extension UIColor {
    
    /// Initialize from integral RGB values (+ alpha channel in range 0-100)
    convenience init(rgb: UInt8..., alpha: UInt = 100) {
        self.init(
            red: CGFloat(rgb[0]) / 255,
            green: CGFloat(rgb[1]) / 255,
            blue: CGFloat(rgb[2]) / 255,
            alpha: CGFloat(min(alpha, 100)) / 100
        )
    }
    
    convenience init(_ hex: UInt) {
        self.init(
            red: CGFloat((hex & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((hex & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(hex & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
