//
//  BaseViewModel.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxSwift
import RxCocoa

class BaseViewModel: BaseRXViewModelProtocol {
    let activityIndicatorStatePublishRelay = PublishRelay<ActivityIndicatorState>()
    let errorDisplayPublishRelay = PublishRelay<AppError>()
    let disposeBag = DisposeBag()
}
