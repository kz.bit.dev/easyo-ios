//
//  BaseRXViewModelProtocol.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

protocol BaseRXViewModelProtocol: class {
    var activityIndicatorStatePublishRelay: PublishRelay<ActivityIndicatorState> { get }
    var errorDisplayPublishRelay: PublishRelay<AppError> { get }
}

enum ActivityIndicatorState {
    case show
    case hide
}
