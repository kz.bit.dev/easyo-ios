//
//  BaseRXViewController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import RxSwift

class BaseRXViewController: UIViewController, BaseViewControllerProtocol, BaseRXViewControllerProtocol {
    
    /// ViewModel должен подписываться под этот протокол, в инициализации указать self.baseViewModel = viewModel
    var baseViewModel: BaseRXViewModelProtocol!
    var disposeBag = DisposeBag()
    
    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge)
        activityIndicator.color = UIColor(hexString: "#B5B5B5")
        activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return activityIndicator
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        if baseViewModel == nil {
            baseViewModel = BaseViewModel()
        }
        setupDefaults()
    }
}
