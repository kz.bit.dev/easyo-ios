//
//  NavigationController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, BaseViewControllerProtocol {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(
            style: UIActivityIndicatorView.Style.whiteLarge
        )
        activityIndicator.color = UIColor(hexString: "#B5B5B5")
        activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        return activityIndicator
    }()
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        let backBarButtonItem = UIBarButtonItem()
        backBarButtonItem.title = nil
        viewControllers.last?.navigationItem.backBarButtonItem = backBarButtonItem
        super.pushViewController(viewController, animated: animated)
    }
}

extension UINavigationController {
   open override var preferredStatusBarStyle: UIStatusBarStyle {
      return topViewController?.preferredStatusBarStyle ?? .default
   }
}
