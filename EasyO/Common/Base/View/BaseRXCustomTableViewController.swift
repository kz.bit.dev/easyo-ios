//
//  BaseRXCustomTableViewController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class BaseRXCustomTableViewController: BaseRXViewController {
    
    var tableView = UITableView()
    lazy var topConstant = Constants.statusBarHeight + (navigationController?.navigationBar.frame.height ?? 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTableView()
        setTableViewConstraints()
        stylizeTableView()
    }
}

extension BaseRXCustomTableViewController {
    func addTableView() {
        view.addSubview(tableView)
    }
    
    func setTableViewConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: topConstant),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(layoutConstraints)
    }
    
    func stylizeTableView() {
        tableView.backgroundColor = AppColor.whitish.uiColor
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        if #available(iOS 11.0, *) {
            tableView.contentInset.bottom = view.safeAreaInsets.bottom
        }
    }
}
