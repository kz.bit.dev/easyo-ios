//
//  BaseViewControllerProtocol.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol BaseViewControllerProtocol: class {
    var activityIndicator: UIActivityIndicatorView { get }
}

extension BaseViewControllerProtocol where Self: UIViewController {
    
    func showSuccess(message: String, completion: VoidCompletion?) {
        let alert = UIAlertController(title: "Сообщение", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Закрыть", style: .cancel) { _ in
            completion?()
        }
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func showSuccessWithGif(title: String, message: String, completion: VoidCompletion? = nil) {
        
        view.endEditing(true)
        
    }
    
    func showOption(message: String, onOptionSelect perform: @escaping (_ isOkOptionSelected: Bool) -> Void) {
        let alertController = UIAlertController(title: "Внимание", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in perform(true) }
        let cancel = UIAlertAction(title: "Отмена", style: .cancel) { _ in perform(false) }
        alertController.addAction(okAction)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
    
    
    func showError(error: AppError, completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: "Внимание", message: error.description, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Закрыть", style: .cancel) { _ in
            completion?()
        }
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func showActivityIndicator() {
        if let tabBarController = tabBarController {
            activityIndicator.frame = CGRect(
                x: 0,
                y: 0,
                width: tabBarController.view.frame.width,
                height: tabBarController.view.frame.height
            )
            tabBarController.view.addSubview(activityIndicator)
        } else if let navigationController = navigationController {
            activityIndicator.frame = CGRect(
                x: 0,
                y: 0,
                width: navigationController.view.frame.width,
                height: navigationController.view.frame.height
            )
            navigationController.view.addSubview(activityIndicator)
        } else {
            activityIndicator.frame = CGRect(
                x: 0,
                y: 0,
                width: view.frame.width,
                height: view.frame.height
            )
            view.addSubview(activityIndicator)
        }
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
}

/// Closure, ничего не принимает и возвращает Void
typealias VoidCompletion = (() -> Void)

/// Тип заголовка сети
typealias NetworkHeaderType = [String: String]

/// Тип параметра сети
typealias NetworkParameterType = [String: Any]
