//
//  BaseRXViewControllerProtocol.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol BaseRXViewControllerProtocol: BaseViewControllerProtocol {
    var baseViewModel: BaseRXViewModelProtocol! { get }
    var disposeBag: DisposeBag { get }
}

extension BaseRXViewControllerProtocol where Self: UIViewController {
    func setupDefaults() {
        baseViewModel.activityIndicatorStatePublishRelay.subscribe(onNext: { [unowned self] state in
            switch state {
            case .show:
                self.showActivityIndicator()
            case .hide:
                self.hideActivityIndicator()
            }
        }).disposed(by: disposeBag)
        
        baseViewModel.errorDisplayPublishRelay.subscribe(onNext: { [unowned self] error in
            self.showError(error: error)
        }).disposed(by: disposeBag)
    }
}
