//
//  ViewInstallationProtocol.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

protocol ViewInstallationProtocol {
    func setupViews()
    func addSubviews()
    func setViewConstraints()
    func stylizeViews()
}

extension ViewInstallationProtocol {
    func setupViews() {
        addSubviews()
        setViewConstraints()
        stylizeViews()
    }
}
