//
//  Constants.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

struct Constants {
    
    private init() {}
    
    static let screenSize = UIScreen.main.bounds.size
    static let width = UIScreen.main.bounds.size.width
    static let heigth = UIScreen.main.bounds.size.height
    static let statusBarHeight = UIApplication.shared.statusBarFrame.height
    
    static func modelIdentifier() -> String {
        if let simulatorModelIdentifier = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] { return simulatorModelIdentifier }
        var sysinfo = utsname()
        uname(&sysinfo)
        return String(
            bytes: Data(bytes: &sysinfo.machine, count: Int(_SYS_NAMELEN)),
            encoding: .ascii
        )!.trimmingCharacters(
            in: .controlCharacters
        )
    }
    
    static let deviceUUID: String? = UIDevice.current.identifierForVendor?.uuidString
    static let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    static let systemVersion = UIDevice.current.systemVersion
}
