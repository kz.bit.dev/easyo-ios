//
//  MaskedTextField.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import InputMask

class MaskedTextField: UITextField {
    
    private let maskConfiguration = MaskedTextFieldDelegate()
    
    var valueWithoutMask: String = ""
    var innerShadow = true
    
    var maskString: String {
        set {
            maskConfiguration.primaryMaskFormat = newValue
        }
        get {
            return maskConfiguration.primaryMaskFormat
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    func commonInit() {
        delegate = maskConfiguration
        layer.masksToBounds = true
        maskConfiguration.listener = self
        maskConfiguration.affinityCalculationStrategy = .prefix
        maskString = "[…]"
    }
    
    func set(text: String) {
        let value = maskConfiguration.put(text: text, into: self, autocomplete: true)
        print(value)
    }
    
    override var bounds: CGRect {
        didSet {
            guard innerShadow else { return }
            addInnerShadow()
        }
    }
    
    private func addInnerShadow() {
        let innerShadow = CALayer()
        innerShadow.frame = bounds

        let radius: CGFloat = 15
        let path = UIBezierPath(roundedRect: innerShadow.bounds.insetBy(dx: -1, dy: -1), cornerRadius: radius)
        let cutout = UIBezierPath(roundedRect: innerShadow.bounds, cornerRadius: radius).reversing()

        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        
        innerShadow.shadowColor = UIColor.black.cgColor
        innerShadow.shadowOffset = CGSize(width: 0, height: 3)
        innerShadow.shadowOpacity = 0.15
        innerShadow.shadowRadius = 3
        innerShadow.cornerRadius = self.frame.size.height/2
        layer.addSublayer(innerShadow)
    }
}

extension MaskedTextField: MaskedTextFieldDelegateListener {
    func textField(_ textField: UITextField, didFillMandatoryCharacters complete: Bool, didExtractValue value: String) {
        valueWithoutMask = value
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignFirstResponder()
        return false
    }
}
