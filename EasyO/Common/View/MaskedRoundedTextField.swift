//
//  MaskedRoundedTextField.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MaskedRoundedTextField: RoundedView {
    
    let textfield = UITextField()
    private let strokeView = GradientView()
    
    var delegate: UITextFieldDelegate? {
        set {
            textfield.delegate = newValue
        } get {
            return textfield.delegate
        }
    }
    
    var text: String {
        set {
            textfield.text = newValue
        } get {
            return textfield.text ?? ""
        }
    }
    
    var placeholder: String? {
        set {
            textfield.placeholder = newValue
        } get {
            return textfield.placeholder
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(cornerRadius: CGFloat) {
        strokeView.cornerRadius = cornerRadius
        textfield.layer.cornerRadius = cornerRadius
    }
}

extension MaskedRoundedTextField: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(strokeView)
        addSubview(textfield)
    }
    
    func setViewConstraints() {
        strokeView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        
        textfield.makeEdgeConstraints(equal: self, offset: 1.5)
        
        layoutIfNeeded()
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        clipsToBounds = false
        
        strokeView.startColor = UIColor.white
        strokeView.endColor = UIColor(hexString: "#CCD9E4")
        strokeView.cornerRadius = 15
        strokeView.endPointX = 1
        strokeView.endPointY = 1
        
        textfield.borderStyle = .none
        textfield.layer.cornerRadius = 15
        textfield.autocorrectionType = .no
        textfield.autocapitalizationType = .none
        textfield.backgroundColor = UIColor(hexString: "#DEEBF6")
        textfield.setLeftPaddingPoints(8)
        textfield.setRightPaddingPoints(8)
    }
}
