//
//  ScrollView.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class ScrollView: UIScrollView {
    
    let contentView = UIView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(contentView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
