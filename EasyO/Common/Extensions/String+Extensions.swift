//
//  String+Extensions.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

extension String {
    func date(withFormat dateFormat: DateFormat) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.string
        return dateFormatter.date(from: self)
    }
    
    subscript(value: NSRange) -> Substring {
        return self[value.lowerBound..<value.upperBound]
    }
    
    subscript(value: CountableClosedRange<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)...index(at: value.upperBound)]
        }
    }
    
    subscript(value: CountableRange<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)..<index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeUpTo<Int>) -> Substring {
        get {
            return self[..<index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeThrough<Int>) -> Substring {
        get {
            return self[...index(at: value.upperBound)]
        }
    }
    
    subscript(value: PartialRangeFrom<Int>) -> Substring {
        get {
            return self[index(at: value.lowerBound)...]
        }
    }
    
    func index(at offset: Int) -> String.Index {
        return index(startIndex, offsetBy: offset)
    }
    
    func toPhoneNumber() -> String {
        let digits = self
        if digits.count == 12 {
            return digits.replacingOccurrences(
                of: "(\\d{1})(\\d{3})(\\d{3})(\\d{2})(\\d{2})",
                with: "$1 $2 $3 $4 $5",
                options: .regularExpression,
                range: nil
            )
        }
        return self
    }
    
    func toBirthday() -> String {
        let digits = self
        if digits.count == 8 {
            return digits.replacingOccurrences(
                of: "(\\d{2})(\\d{2})(\\d{4})",
                with: "$1.$2.$3",
                options: .regularExpression,
                range: nil
            )
        }
        return self
    }
}
