//
//  CALayer+Extensions.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

extension CALayer {
    func dropShadow(
        shadowColor: UIColor = .black,
        fillColor: UIColor = .white,
        opacity: Float = 0.15,
        offset: CGSize = CGSize(width: 1.5, height: 1.5),
        radius: CGFloat = 2
    ) {
        
        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius).cgPath
        shadowLayer.fillColor = fillColor.cgColor
        shadowLayer.shadowColor = shadowColor.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = offset
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = radius
        insertSublayer(shadowLayer, at: 0)
    }
    
    func removeShadowIfPossible() {
        if !(sublayers?.isEmpty ?? true) {
            sublayers?.remove(at: 0)
        }
    }
}
