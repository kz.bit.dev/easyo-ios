//
//  Date+Extensions.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

extension Date {
    static var dayMonthYearDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM YYYY"
        dateFormatter.locale = Locale(identifier: "ru")
        return dateFormatter
    }
    
    func string(withFormat dateFormat: DateFormat) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.string
        return dateFormatter.string(from: self)
    }
    
    var isInThisYear:  Bool { isInSameYear(date: Date()) }
    
    func isInSameYear (date: Date) -> Bool { isEqual(to: date, toGranularity: .year) }
    
    func isEqual(to date: Date, toGranularity component: Calendar.Component, in calendar: Calendar = .current) -> Bool {
        calendar.isDate(self, equalTo: date, toGranularity: component)
    }
    
    func getDayWithMonth() -> String? {
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "ru_RU")
        if isInThisYear {
            dateFormater.dateFormat = "d MMMM"
        } else {
            dateFormater.dateFormat = "d MMMM, yyyy"
        }
        return dateFormater.string(from: self)
    }
    
    func getDay() -> String? {
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "ru_RU")
        dateFormater.dateFormat = "d"
        return dateFormater.string(from: self)
    }
    
    func getMonth() -> String? {
        let dateFormater = DateFormatter()
        dateFormater.locale = Locale(identifier: "ru_RU")
        dateFormater.dateFormat = "MMMM"
        return dateFormater.string(from: self)
    }
}
