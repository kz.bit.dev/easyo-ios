//
//  UIButton+Extensions.swift
//  EasyO
//
//  Created by Аскар on 6/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

extension UIButton {
    func loadingIndicator(_ show: Bool, title: String = "", color: UIColor = .gray) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.setTitle("", for: .normal)
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.color = color
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.setTitle(title, for: .normal)
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
    
    func animate() {
        UIButton.animate(withDuration: 0.1, animations: {
            self.transform = CGAffineTransform(scaleX: 0.975, y: 0.96)
        }, completion: { _ in
            UIButton.animate(withDuration: 0.1, animations: {
                self.transform = CGAffineTransform.identity
            })
        })
    }
}
