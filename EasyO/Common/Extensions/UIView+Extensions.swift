//
//  UIView+Extensions.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import SkeletonView

extension UIView {
    func setupDefaultClickAnimation(isHighlighted: Bool, completion: ((Bool) -> Void)? = nil) {
        let animationOptions: UIView.AnimationOptions = [.allowUserInteraction]
        if isHighlighted {
            UIView.animate(
                withDuration: 0.1,
                delay: 0,
                usingSpringWithDamping: 1,
                initialSpringVelocity: 0,
                options: animationOptions,
                animations: {
                    self.backgroundColor = AppColor.grayClick.uiColor
                }
            )
        } else {
            UIView.animate(
                withDuration: 0.1,
                delay: 0,
                usingSpringWithDamping: 1,
                initialSpringVelocity: 0,
                options: animationOptions,
                animations: {
                    self.backgroundColor = UIColor.white
                }
            )
        }
    }
    
    func makeEdgeConstraints(equal view: UIView, offset: CGFloat = 0.0) {
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.topAnchor, constant: offset).isActive = true
        bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -offset).isActive = true
        leftAnchor.constraint(equalTo: view.leftAnchor, constant: offset).isActive = true
        rightAnchor.constraint(equalTo: view.rightAnchor, constant: -offset).isActive = true
    }
    
    func makeCornerRadius(corners: UIRectCorner, cornerRadii: CGFloat) {
        let path = UIBezierPath(
            roundedRect: self.bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: cornerRadii, height: cornerRadii)
        )
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func showSkeletonAnimation(usingColor color: UIColor = SkeletonAppearance.default.tintColor, animation: SkeletonLayerAnimation? = nil) {
        showAnimatedSkeleton(usingColor: color, animation: animation)
    }
    
    func shakeView(
        duration: TimeInterval = 0.05,
        repeatCount: Float = 5,
        distance: CGFloat = 5,
        completion: (() -> Void)? = nil
    ) {
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = duration
        animation.repeatCount = repeatCount
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: center.x - distance, y: center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: center.x + distance, y: center.y))
        
        CATransaction.setCompletionBlock(completion)
        layer.add(animation, forKey: "position")
        CATransaction.commit()
    }
    
    func setCorners(corners: UIRectCorner, withRadius: CGFloat){
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.frame
        rectShape.position = self.center
        rectShape.path = UIBezierPath(
            roundedRect: self.bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: withRadius, height: withRadius)
        ).cgPath
        self.layer.mask = rectShape
    }
    
    func setAllSideShadow(shadowShowSize: CGFloat = 1.0) {
        let shadowSize : CGFloat = shadowShowSize
        let shadowPath = UIBezierPath(
            roundedRect: CGRect(
                x: -shadowSize / 2,
                y: -shadowSize / 2,
                width: self.frame.size.width + shadowSize,
                height: self.frame.size.height + shadowSize
            ),
            cornerRadius: 15
        )
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.lightGray.withAlphaComponent(0.8).cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.5
        self.layer.shadowPath = shadowPath.cgPath
        self.layer.cornerRadius = 15
    }
}
