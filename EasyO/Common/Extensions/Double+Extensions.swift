//
//  Double+Extensions.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

extension Double {
    func convertToMinutesSecondsString() -> String {
        let hours = Int(self) / 3600 % 3600
        let minutes = Int(self) / 60 % 60
        let seconds = Int(self) % 60
        if hours > 0 {
            return "Отправить повторно через " + String(format:"%02i:%02i:%02i", hours, minutes, seconds)
        }
        return "Отправить повторно через " + String(format:"%02i:%02i", minutes, seconds)
    }
}
