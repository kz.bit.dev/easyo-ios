//
//  Dictionary+Extensions.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

extension Dictionary {
    var jsonStringRepresentation: NSString? {
        guard let theJSONData = try? JSONSerialization.data(
            withJSONObject: self,
            options: [.prettyPrinted]
        ) else {
            return nil
        }

        guard let object = try? JSONSerialization.jsonObject(with: theJSONData, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
              else { return nil }
        
        return prettyPrintedString
    }
}
