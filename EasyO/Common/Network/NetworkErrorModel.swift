//
//  NetworkErrorModel.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct NetworkErrorModel: Codable {
    let message: String
    let code: String
}
