//
//  Route.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

enum Route {
    
    case sessionsAuth
    case sessionsLogout
    
    case tables
    
    case menus
    
    case meals
    
    case orders
    case ordersById(id: Int)
    case ordersPriceById(id: Int)
    
    case orderLines
    case orderLineById(id: Int)
    
    case websocket
    
    case bills
    
    case uploadStorage
    
    case usersBy(id: Int)
    
    case sessionsFcm(fcm: String)
    
    func getRoute() -> String {
        switch self {
        case .sessionsAuth:
            return "sessions/auth"
        case .sessionsLogout:
            return "sessions/logout"
            
        case .tables:
            return "tables"
            
        case .menus:
            return "menus"
            
        case .meals:
            return "meals"
            
        case .orders:
            return "orders"
        case .ordersById(let id):
            return "orders/" + id.description
        case .ordersPriceById(let id):
            return "orders/price/" + id.description
            
        case .orderLines:
            return "order_lines"
        case .orderLineById(let id):
            return "order_lines/" + id.description
            
        case .websocket:
            return "ws/websocket"
            
        case .bills:
            return "bills"
        
        case .uploadStorage:
            return "upload/storage/v1/b/easyo/o"
        
        case .usersBy(let id):
            return "users/" + id.description
            
        case .sessionsFcm(let fcm):
            return "sessions/fcm?fcmToken=" + fcm
        }
    }
}

extension Route: URLConvertible {
    func url() -> URL? {
        switch self {
        case .websocket:
            return URL(string: "https://easyo.herokuapp.com/" + self.getRoute())
        default:
            return URL(string: "https://easyo.herokuapp.com/api/" + self.getRoute())
        }
    }
    
    func urlString() -> String {
        switch self {
        case .uploadStorage:
            return "https://storage.googleapis.com/" + self.getRoute()
        default:
            return "https://easyo.herokuapp.com/api/" + self.getRoute()
        }
    }
}
