//
//  MultipartFormDataNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/30/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

protocol MultipartFormDataNetworkContext: NetworkContext {
    var data: Data? { get }
}
