//
//  NetworkContext.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

protocol NetworkContext: class {
    var route: Route { get }
    var method: NetworkMethod { get }
    var encoding: NetworkEncoding { get }
    var parameters: [String: Any] { get set }
    var headers: NetworkHeaderType { get }
}

extension NetworkContext {
    
    var url: URL? { return route.url() }
    
    var encoding: NetworkEncoding { return .url }
    
    var headers: [String: String] {
        var headers = NetworkHeaderType()
        headers["Content-type"] = "application/json"
        return headers
    }
}
