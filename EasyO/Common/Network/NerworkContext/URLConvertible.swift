//
//  URLConvertible.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

protocol URLConvertible {
    func url() -> URL?
}
