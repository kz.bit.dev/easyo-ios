//
//  AppRequestAdapter.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Alamofire

class AppRequestAdapter: RequestAdapter {
    
    var token: String?
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var adaptedRequest = urlRequest
        if let accessToken = token {
            adaptedRequest.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        return adaptedRequest
    }
}
