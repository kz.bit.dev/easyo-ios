//
//  SucceessNetworkResponse.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

class SucceessNetworkResponse: NetworkResponse {
    var data: Data?
    var networkError: NetworkError? = nil
    
    init(data: Data?) {
        self.data = data
    }
}
