//
//  NetworkAdapter.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Alamofire
import SystemConfiguration

class NetworkAdapter: NetworkService {
    func load(networkContext: NetworkContext, completion: @escaping (NetworkResponse) -> Void) {
        guard let url = networkContext.url else {
            completion(FailureNetworkResponse(networkError: .urlInvalid))
            return
        }
        
        guard networkConnectionStatus != .notAvailable else {
            completion(FailureNetworkResponse(networkError: .noConnection))
            return
        }
        
        let dataRequest = sessionManager.request(
            url,
            method: httpMethod(method: networkContext.method),
            parameters: parameters(networkParameters: networkContext.parameters),
            encoding: encoding(encoding: networkContext.encoding),
            headers: headers(networkHeaders: networkContext.headers)
        )
        
        dataRequest.validate().responseData { [weak self] serverResponse in
            guard let strongSelf = self else { return }
            strongSelf.logForRequest(serverResponse: serverResponse, networkContext: networkContext)
            strongSelf.log(response: serverResponse)
            let networkResponse = strongSelf.dataResponseToNetworkResponse(response: serverResponse)
            completion(networkResponse)
        }
    }
    
    func loadDecodable<T: Decodable>(networkContext: NetworkContext, type: T.Type, completion: @escaping (Result<T>) -> Void) {
        load(networkContext: networkContext) { networkResponse in
            if let error = networkResponse.networkError {
                completion(.error(error))
                return
            }
            guard let result: T = networkResponse.decode() else {
                completion(.error(NetworkError.dataLoad))
                return
            }
            completion(.success(result))
        }
    }
    
    func loadDecodable<T: Decodable>(
        context: MultipartFormDataNetworkContext,
        type: T.Type,
        completion: @escaping (Result<T>) -> Void
    ) {
        load(context: context) { networkResponse in
            if let error = networkResponse.networkError {
                completion(.error(error))
                return
            }
            guard let result: T = networkResponse.decode() else {
                completion(.error(NetworkError.dataLoad))
                return
            }
            completion(.success(result))
        }
    }
    
    func load(
        context: MultipartFormDataNetworkContext,
        completion: @escaping (_ networkResponse: NetworkResponse) -> Void
    ) {
        guard networkConnectionStatus != .notAvailable else {
            completion(FailureNetworkResponse(networkError: .noConnection))
            return
        }
        
        var urlString = context.route.urlString()
        
        for (index, element) in context.parameters.enumerated() {
            if (index == 0) {
                urlString += "?\(element.key)=\(element.value)"
            } else {
                urlString += "&\(element.key)=\(element.value)"
            }
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)

        let url = URL(string: urlString)!
        var urlRequest = try! URLRequest(url: url, method: .post)

        guard let postData = context.data else {
            return
        }

        urlRequest.httpBody = postData

        let task = session.dataTask(with: urlRequest) { data, response, error in
            if let error = error {
                print(error)
                completion(FailureNetworkResponse(networkError: .serverError(description: error.localizedDescription)))
            } else if let data = data {
                let string = String(decoding: data, as: UTF8.self)
                print(string)
                completion(SucceessNetworkResponse(data: data))
            } else {
                completion(FailureNetworkResponse(networkError: .dataLoad))
            }
        }

        task.resume()
    }
    
    private func dataResponseToNetworkResponse(response: DataResponse<Data>) -> NetworkResponse {
        print(response.response?.statusCode as Any)
        guard response.response?.statusCode == 200 else {
            if response.response?.statusCode == 403 {
                LocalStore().removeValue(forKey: .token)
                guard let window = UIApplication.shared.keyWindow else {
                    return FailureNetworkResponse(networkError: .dataLoad)
                }
                window.rootViewController = OnboardingViewController()
                return FailureNetworkResponse(networkError: .dataLoad)
            }
            guard let data = response.data,
                let errorModel = try? JSONDecoder().decode(NetworkErrorModel.self, from: data)
                else {
                    return FailureNetworkResponse(networkError: .dataLoad)
                }
            
            return FailureNetworkResponse(networkError: .serverError(description: errorModel.message))
        }
        
        guard let data = response.data else {
            return FailureNetworkResponse(networkError: .dataLoad)
        }
        let successNetworkResponse = SucceessNetworkResponse(data: data)
        return successNetworkResponse
    }
    
    private func httpMethod(method: NetworkMethod) -> HTTPMethod {
        switch method {
        case .get:
            return .get
        case .post:
            return .post
        case .put:
            return .put
        }
    }
    
    private func parameters(networkParameters: NetworkParameterType) -> Parameters {
        var parameters = Parameters()
        parameters = networkParameters
        return parameters
    }
    
    private func encoding(encoding: NetworkEncoding) -> ParameterEncoding {
        switch encoding {
        case .json:
            return JSONEncoding.default
        case .url:
            return URLEncoding.default
        }
        
    }
    
    private func headers(networkHeaders: NetworkHeaderType) -> HTTPHeaders {
        var headers = HTTPHeaders()
        for header in networkHeaders {
            headers[header.key] = header.value
        }
        return headers
    }
    
    private enum NetworkConnectionStatus { case notAvailable, availableViaWWAN, availableViaWiFi }
    
    private var networkConnectionStatus: NetworkConnectionStatus {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }
        guard let target = defaultRouteReachability else { return .notAvailable }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(target, &flags) { return .notAvailable }
        
        /* The target host is not reachable: */
        if !flags.contains(.reachable) { return .notAvailable }
            /* WWAN connections are OK if the calling application is using the CFNetwork APIs: */
        else if flags.contains(.isWWAN) { return .availableViaWWAN }
            /* If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi: */
        else if !flags.contains(.connectionRequired) { return .availableViaWiFi }
            /* The connection is on-demand (or on-traffic) if the calling application is
             using the CFSocketStream or higher APIs and no [user] intervention is needed: */
        else if (flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)) &&
            !flags.contains(.interventionRequired) { return .availableViaWiFi }
            /* else: */
        else { return .notAvailable }
    }
    
    private func log(response: DataResponse<Data>) {
        print("--------URL------------")
        print(response.request?.url?.absoluteString ?? "nil")
        if let error = response.error {
            print("----------Error----------")
            print(error.localizedDescription)
        }
        if let data = response.data {
            print("--------DataJson-------")
            print(data.prettyPrintedJSONString as Any)
        } else {
            print("Data is nil")
        }
        print("--------END------------")
    }
    
    private func logForRequest(serverResponse: DataResponse<Data>, networkContext: NetworkContext) {
        print("-------------HEADER-----------")
        if networkContext.url == serverResponse.request?.url {
            if let jsonData = try? JSONSerialization.data(
                withJSONObject: networkContext.headers,
                options: .prettyPrinted
            ) {
                print(jsonData.prettyPrintedJSONString as Any)
            }
        }
        print("-------------Body-------------")
        if networkContext.url == serverResponse.request?.url {
            if let jsonData = try? JSONSerialization.data(
                withJSONObject: networkContext.parameters,
                options: .prettyPrinted
            ) {
                print(jsonData.prettyPrintedJSONString as Any)
            }
        }
    }
    
    private func generatePrettyPtintedJson(data: Data) -> NSString? {
        guard let object = try? JSONSerialization.jsonObject(with: data, options: []),
              let data = try? JSONSerialization.data(withJSONObject: object, options: [.prettyPrinted]),
              let prettyPrintedString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
              else { return nil }
        
        return prettyPrintedString
    }
}
