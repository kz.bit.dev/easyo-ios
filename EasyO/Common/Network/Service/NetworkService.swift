//
//  NetworkService.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

protocol NetworkService {
    func load(
        networkContext: NetworkContext,
        completion: @escaping (NetworkResponse) -> Void
    )
    func loadDecodable<T: Decodable>(
        networkContext: NetworkContext,
        type: T.Type,
        completion: @escaping (Result<T>) -> Void
    )
    func load(
        context: MultipartFormDataNetworkContext,
        completion: @escaping (_ networkResponse: NetworkResponse) -> Void
    )
    func loadDecodable<T: Decodable>(
        context: MultipartFormDataNetworkContext,
        type: T.Type,
        completion: @escaping (Result<T>) -> Void
    )
}
