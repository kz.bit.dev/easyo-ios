//
//  AppSessionManager.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Alamofire

let sessionManager = AppSessionManager()

class AppSessionManager: SessionManager {
    func set(adapter: RequestAdapter) {
        self.adapter = adapter
    }
}
