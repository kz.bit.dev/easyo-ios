//
//  SocketService.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation
import StompClientLib

class SocketService {
    
    private var socketClient = StompClientLib()
    private var token: String? = nil
    private var reconectTimer: Timer? = nil
    
    private let ordersUpdateSubscribePath = "/orders/update"
    private let orderLinesUpdateSubscribePath = "/order_lines/update"
    private let billUpdateSubscribePath = "/bills/update"
    
    func openConnection(token: String) {
        self.token = token
        connect()
    }
    
    private func connect() {
        print("Socket start connect")
        var socketRequest: NSURLRequest {
            let webSocket = Route.websocket.url()!
            var request = URLRequest(url: webSocket)
            request.setValue(token, forHTTPHeaderField: "Authorization")
            return request as NSURLRequest
        }
        
        socketClient.openSocketWithURLRequest(
            request: socketRequest,
            delegate: self,
            connectionHeaders: [
                "accept-version":"1.1,1.2"
            ]
        )
    }
    
    func receivedMessage(destination: String, jsonBody: AnyObject?) {
        guard jsonBody != nil else { return }
        ClientSingleton.shared.loadTables { _ in }
        ClientSingleton.shared.loadOrders { _ in }
        ClientSingleton.shared.loadOrderLines { _ in }
        guard let jsonData = jsonBody?.data else { return }
        switch destination {
        case ordersUpdateSubscribePath:
            guard let model = try? JSONDecoder().decode(
                OrderModel.self,
                from: jsonData
            ) else { return }
            print(model)
            
        case orderLinesUpdateSubscribePath:
            guard let orderLine = try? JSONDecoder().decode(
                 OrderLineListItem.self,
                from: jsonData
            ) else { return }
            print(orderLine)
        default:
            return
        }
    }
}

extension SocketService: StompClientLibDelegate {
    func stompClient(
        client: StompClientLib!,
        didReceiveMessageWithJSONBody jsonBody: AnyObject?,
        akaStringBody stringBody: String?,
        withHeader header: [String : String]?,
        withDestination destination: String
    ) {
        receivedMessage(destination: destination, jsonBody: jsonBody)
    }
    
    func stompClient(client: StompClientLib!, didReceiveMessageWithJSONBody jsonBody: AnyObject?, withHeader header: [String : String]?, withDestination destination: String) {
        receivedMessage(destination: destination, jsonBody: jsonBody)
    }

    func stompClientJSONBody(
        client: StompClientLib!,
        didReceiveMessageWithJSONBody jsonBody: String?,
        withHeader header: [String : String]?,
        withDestination destination: String
    ) {
        receivedMessage(destination: destination, jsonBody: jsonBody as AnyObject?)
    }

    func stompClientDidDisconnect(client: StompClientLib!) {
        reconectTimer?.invalidate()
        reconectTimer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { timer in
            if (!self.socketClient.isConnected()) {
                print("Socket start connect")
                self.connect()
            }
        }
    }

    func stompClientDidConnect(client: StompClientLib!) {
        reconectTimer?.invalidate()
        socketClient.subscribe(destination: ordersUpdateSubscribePath)
        socketClient.subscribe(destination: orderLinesUpdateSubscribePath)
        socketClient.subscribe(destination: billUpdateSubscribePath)
    }
    
    func serverDidSendReceipt(client: StompClientLib!, withReceiptId receiptId: String) {
    }

    func serverDidSendError(
        client: StompClientLib!,
        withErrorMessage description: String,
        detailedErrorMessage message: String?
    ) {
        print("Server did send error -> ", description)
    }

    func serverDidSendPing() {
        print("Server did send ping")
    }
}
