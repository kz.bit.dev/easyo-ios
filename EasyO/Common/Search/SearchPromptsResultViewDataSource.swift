//
//  SearchPromptsResultViewDataSource.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol SearchPromptsResultViewDataSource: class {
    func searchPromptsResultViewNumberOOfSections(in tableView: UITableView) -> Int
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        numberOfRowsInSection section: Int
    ) -> Int
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell
}

extension SearchPromptsResultViewDataSource {
    func searchPromptsResultViewNumberOOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}
