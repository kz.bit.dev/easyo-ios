//
//  SearchPromptsResultViewDelegate.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol SearchPromptsResultViewDelegate: class {
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    )
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        heightForRowAt indexPath: IndexPath
    ) -> CGFloat
}

extension SearchPromptsResultViewDelegate {
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        didSelectRowAt indexPath: IndexPath
    ) {}
    func searchPromptsResultView(
        _ searchPromptsResultView: SearchPromptsResultViewController,
        tableView: UITableView,
        heightForRowAt indexPath: IndexPath
    ) -> CGFloat { return 64 }
}
