//
//  SearchController.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class SearchController: UISearchController {
    
    lazy private var customSearchBar = SearchBar()
}

extension SearchController: UISearchBarDelegate {
    
    override var searchBar: UISearchBar { return customSearchBar }
}

class SearchBar: UISearchBar {
    
}
