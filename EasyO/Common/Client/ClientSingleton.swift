//
//  ClientSingleton.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class ClientSingleton {
    
    static let shared = ClientSingleton()
    
    let currentUserRelay = BehaviorRelay<AuthUser?>(value: nil)
    let tablesRelay = BehaviorRelay<[TableModel]>(value: [])
    let menusRelay = BehaviorRelay<[MenuModel]>(value: [])
    let ordersRelay = BehaviorRelay<[OrderModel]>(value: [])
    let orderLinesRelay = BehaviorRelay<[OrderLineListItem]>(value: [])
    
    private let networkService: NetworkService = NetworkAdapter()
    private var tables = [TableModel]()
    private var menus = [MenuModel]()
    private var orders = [OrderModel]()
    private var orderLines = [OrderLineListItem]()
    private var mealsDictionary = [Int: MealModel]()
    
    private init() {}
    
    func getTableStatus(tableId: Int) -> TableStatusType {
        guard let table = tables.first(where: { $0.id == tableId }) else { return .empty }
        guard let order = table.order else { return .empty }
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return .empty }
        if order.acceptedUser.id == user.id && order.table.id == tableId {
            return .mine
        }
        return .another
    }
    
    func getTableNumber(tableId: Int) -> String? {
        guard let table = tables.first(where: { $0.id == tableId }) else { return nil }
        return table.number
    }
    
    func getTableId(tableNumber: String?) -> Int? {
        guard let tableNumber = tableNumber else { return nil }
        guard let table = tables.first(where: { $0.number == tableNumber }) else { return nil }
        return table.id
    }
    
    func getMealById(id: Int) -> MealModel? {
        return mealsDictionary[id]
    }

    func loadData(completion: @escaping (Result<Void>) -> Void) {
        var results = [Result<Void>]()
        
        if let user: AuthUser = LocalStore().getValue(forKey: .currentUser) {
            currentUserRelay.accept(user)
        }
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        loadTables { result in
            results.append(result)
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        loadMenu { result in
            results.append(result)
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        loadOrders { result in
            results.append(result)
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        loadOrderLines { result in
            results.append(result)
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            completion(.success(()))
        }
    }
    
    func loadMenu(completion: @escaping (Result<Void>) -> Void) {
        let networkContext = MenusNetworkContext()
        networkService.loadDecodable(
            networkContext: networkContext,
            type: PaginationData<MenuModel>.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                strongSelf.menus = data.data
                strongSelf.menusRelay.accept(data.data)
                for menu in data.data {
                    for meal in menu.mealList {
                        strongSelf.mealsDictionary[meal.id] = meal
                    }
                }
                completion(.success(()))
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
    func loadTables(completion: @escaping (Result<Void>) -> Void) {
        let networkContext = TablesNetworkContext()
        networkService.loadDecodable(
            networkContext: networkContext,
            type: PaginationData<TableModel>.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                strongSelf.tables = data.data
                strongSelf.tablesRelay.accept(strongSelf.tables)
                completion(.success(()))
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
    func loadOrders(completion: @escaping (Result<Void>) -> Void) {
        let networkContext = OrdersNetworkContext()
        networkService.loadDecodable(
            networkContext: networkContext,
            type: PaginationData<OrderModel>.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                strongSelf.orders = data.data
                strongSelf.ordersRelay.accept(data.data)
                completion(.success(()))
            case .error(let error):
                completion(.error(error))
            }
        }
    }
    
    func loadOrderLines(completion: @escaping (Result<Void>) -> Void) {
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        let networkContext: OrderLinesNetworkContext
        switch user.userPositionEnum {
        case .cook:
            networkContext = OrderLinesNetworkContext(page: 0, orderLineStatusEnum: .prepare)
        default:
            networkContext = OrderLinesNetworkContext(page: 0, orderLineStatusEnum: .awaiting)
        }
        networkService.loadDecodable(
            networkContext: networkContext,
            type: PaginationData<OrderLineListItem>.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                strongSelf.orderLines = data.data
                strongSelf.orderLinesRelay.accept(data.data)
                completion(.success(()))
            case .error(let error):
                completion(.error(error))
            }
        }
    }
}
