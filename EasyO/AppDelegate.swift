//
//  AppDelegate.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        
        setupPushNotification(application: application)
        
        window = UIWindow()
        window?.makeKeyAndVisible()

        if let showed: Bool = LocalStore().getValue(forKey: .onboardingShowed), showed {
            if let token: String = LocalStore().getValue(forKey: .token),
               let user: AuthUser = LocalStore().getValue(forKey: .currentUser) {
                
                let adapter = AppRequestAdapter()
                adapter.token = token
                sessionManager.set(adapter: adapter)
                
                let userType: UserType
                switch user.userPositionEnum {
                case .cook:
                    userType = .cook
                default:
                    userType = .waiter
                }
                
                window?.rootViewController = MainTabbarController(userType: userType)
            } else {
                LocalStore().removeAllValues()
                window?.rootViewController = OnboardingViewController()
            }
        } else {
            window?.rootViewController = OnboardingViewController()
        }
        return true
    }
    
    func application(
        _ application: UIApplication,
        didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
    ) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    private func setupPushNotification(application: UIApplication) {
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { _, _ in }
        application.registerForRemoteNotifications()
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        print(fcmToken)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        completionHandler([.alert, .badge, .sound])
    }

    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        completionHandler()
    }
}
