//
//  ProfileViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class ProfileViewModel: BaseViewModel {

    let userRelay = PublishRelay<AuthUser>()
    let componentsRelay = PublishRelay<[ProfileComponent]>()
    let logoutRelay = PublishRelay<Void>()
    
    private let networkService: NetworkService = NetworkAdapter()
    
    override init() {
        super.init()
        
        ClientSingleton.shared.currentUserRelay.subscribe(onNext: { [unowned self] user in
            guard let user = user else { return }
            LocalStore().set(value: user, forKey: .currentUser)
            self.userRelay.accept(user)
        }).disposed(by: disposeBag)
    }
    
    func loadComponents() {
        var components = [ProfileComponent]()
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else {
            components.append(.init(type: .logout, description: nil))
            componentsRelay.accept(components)
            return
        }
        userRelay.accept(user)
        components.append(.init(type: .role, description: user.userPositionEnum?.title))
        components.append(.init(type: .email, description: user.email))
        components.append(.init(type: .logout, description: nil))
        componentsRelay.accept(components)
    }
    
    func logout() {
        let networkContext = SessionsLogoutNetworkContext()
        networkService.load(networkContext: networkContext) { [weak self] repsonse in
            guard let strongSelf = self else { return }
            LocalStore().removeAllValues()
            strongSelf.logoutRelay.accept(())
        }
    }
}
