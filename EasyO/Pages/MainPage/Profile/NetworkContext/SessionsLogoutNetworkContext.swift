//
//  SessionsLogoutNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 6/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class SessionsLogoutNetworkContext: NetworkContext {
    
    let route: Route = .sessionsLogout
    let method: NetworkMethod = .put
    var parameters = [String: Any]()
    
}
