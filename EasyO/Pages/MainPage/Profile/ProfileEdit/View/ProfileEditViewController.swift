//
//  ProfileEditViewController.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class ProfileEditViewController: BaseRXViewController {
    
    private let viewModel = ProfileEditViewModel()
    
    private let profileImageView = CachedImageView()
    private let firstNameContainerView = UIStackView()
    private let firstNameTitleLabel = UILabel()
    private let firstNameTextField = MaskedRoundedTextField()
    private let lastNameContainerView = UIStackView()
    private let lastNameTitleLabel = UILabel()
    private let lastNameTextField = MaskedRoundedTextField()
    private let saveButton = UIButton(type: .system)
    
    private var optionalImage: UIImage?
    
    override func loadView() {
        super.loadView()
        baseViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        viewModel.closeRelay.subscribe(onNext: { [unowned self] in
            self.navigationController?.popViewController(animated: true)
        }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        saveButton.setAllSideShadow(shadowShowSize: 1)
    }
}

extension ProfileEditViewController {
    @objc private func profileImageViewTapped() {
        let viewController = UIImagePickerController()
        viewController.delegate = self
        viewController.allowsEditing = true
        
        present(viewController, animated: true, completion: nil)
    }
    
    @objc private func saveTapped() {
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        guard !firstName.isEmpty else {
            firstNameContainerView.shakeView()
            return
        }
        guard !lastName.isEmpty else {
            lastNameContainerView.shakeView()
            return
        }
        viewModel.update(firstName: firstName, lastName: lastName)
    }
}

extension ProfileEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]
    ) {
        
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            selectedImageFromPicker = editedImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        if let image = selectedImageFromPicker {
            optionalImage = image
            viewModel.loadAccountPhoto(image: image)
        }
        DispatchQueue.main.async {
            picker.dismiss(animated: true) {
                self.profileImageView.image = selectedImageFromPicker
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

extension ProfileEditViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(profileImageView)
        view.addSubview(firstNameContainerView)
        view.addSubview(lastNameContainerView)
        view.addSubview(saveButton)
        firstNameContainerView.addArrangedSubview(firstNameTitleLabel)
        firstNameContainerView.addArrangedSubview(firstNameTextField)
        lastNameContainerView.addArrangedSubview(lastNameTitleLabel)
        lastNameContainerView.addArrangedSubview(lastNameTextField)
    }
    
    func setViewConstraints() {
        profileImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(96)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(80)
        }
        
        firstNameContainerView.snp.makeConstraints { make in
            make.top.equalTo(profileImageView.snp.bottom).offset(40)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
        }
        
        lastNameContainerView.snp.makeConstraints { make in
            make.top.equalTo(firstNameContainerView.snp.bottom).offset(24)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
        }
        
        firstNameTextField.snp.makeConstraints { make in
            make.height.equalTo(52)
        }
        
        lastNameTextField.snp.makeConstraints { make in
            make.height.equalTo(52)
        }
        
        saveButton.snp.makeConstraints { make in
            make.top.equalTo(lastNameContainerView.snp.bottom).offset(36)
            make.centerX.equalToSuperview()
            make.height.equalTo(52)
            make.width.equalTo(view.snp.width).offset(-64)
        }
    }
    
    func stylizeViews() {
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        profileImageView.layer.cornerRadius = 40
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.backgroundColor = UIColor(hexString: "#E5E5E5")
        profileImageView.addGestureRecognizer(
            UITapGestureRecognizer(
                target: self,
                action: #selector(profileImageViewTapped)
            )
        )
        profileImageView.isUserInteractionEnabled = true
        
        firstNameContainerView.axis = .vertical
        firstNameContainerView.distribution = .fill
        firstNameContainerView.spacing = 10
        firstNameContainerView.alignment = .fill
        
        firstNameTitleLabel.text = "Имя"
        firstNameTitleLabel.textColor = UIColor(hexString: "#334669")
        firstNameTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        firstNameTextField.textfield.tintColor = .black
        firstNameTextField.textfield.font = .systemFont(ofSize: 17)
        firstNameTextField.textfield.textColor = .black
        firstNameTextField.textfield.attributedPlaceholder = NSAttributedString(
            string: "Имя",
            attributes: [.foregroundColor: UIColor(hexString: "#334669").withAlphaComponent(0.3)]
        )
        
        lastNameContainerView.axis = .vertical
        lastNameContainerView.distribution = .fill
        lastNameContainerView.spacing = 10
        lastNameContainerView.alignment = .fill
        
        lastNameTitleLabel.text = "Фамилия"
        lastNameTitleLabel.textColor = UIColor(hexString: "#334669")
        lastNameTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        lastNameTextField.textfield.tintColor = .black
        lastNameTextField.textfield.font = .systemFont(ofSize: 17)
        lastNameTextField.textfield.textColor = .black
        lastNameTextField.textfield.attributedPlaceholder = NSAttributedString(
            string: "Фамилия",
            attributes: [.foregroundColor: UIColor(hexString: "#334669").withAlphaComponent(0.3)]
        )
        
        saveButton.setTitle("Сохранить", for: .normal)
        saveButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        saveButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        saveButton.addTarget(self, action: #selector(saveTapped), for: .touchUpInside)
        
        if let user: AuthUser = LocalStore().getValue(forKey: .currentUser) {
            firstNameTextField.textfield.text = user.firstName
            lastNameTextField.textfield.text = user.lastName
        }
    }
}
