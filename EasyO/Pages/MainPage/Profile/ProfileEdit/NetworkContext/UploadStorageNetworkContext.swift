//
//  UploadStorageNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/30/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class UploadStorageNetworkContext: MultipartFormDataNetworkContext {
    
    var route: Route = .uploadStorage
    var method: NetworkMethod = .post
    var parameters = [String: Any]()
    var encoding: NetworkEncoding = .url
    var headers = [String: String]()

    var data: Data? {
        let optionalData = image.jpegData(compressionQuality: 1)
        return optionalData
    }
    
    let image: UIImage
    let imageName: String
    
    init(image: UIImage, imageName: String) {
        parameters["uploadType"] = "media"
        parameters["name"] = "users/images/" + imageName
        self.image = image
        self.imageName = imageName
    }
}
