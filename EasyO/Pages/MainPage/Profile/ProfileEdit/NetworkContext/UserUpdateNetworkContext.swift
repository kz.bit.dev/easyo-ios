//
//  UserUpdateNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 6/6/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class UserUpdateNetworkContext: NetworkContext {
    let route: Route
    let method: NetworkMethod = .put
    var parameters = [String: Any]()
    let encoding: NetworkEncoding = .json
    
    init(
        userId: Int,
        firstName: String,
        lastName: String,
        imageUrl: String?
    ) {
        route = .usersBy(id: userId)
        parameters["firstName"] = firstName
        parameters["lastName"] = lastName
        parameters["imageUrl"] = imageUrl
    }
}
