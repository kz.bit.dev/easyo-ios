//
//  ProfileEditViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/30/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class ProfileEditViewModel: BaseViewModel {
    
    let closeRelay = PublishRelay<Void>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var imageLink: String?
    
    func loadAccountPhoto(image: UIImage) {
        let networkContext = UploadStorageNetworkContext(image: image, imageName: UUID().uuidString)
        activityIndicatorStatePublishRelay.accept(.show)
        
        networkService.loadDecodable(
            context: networkContext,
            type: GoogleImageModel.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
            }
            switch result {
            case .success(let imageModel):
                strongSelf.imageLink = imageModel.mediaLink
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
    
    func update(firstName: String, lastName: String) {
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        let networkContext = UserUpdateNetworkContext(
            userId: user.id,
            firstName: firstName,
            lastName: lastName,
            imageUrl: imageLink
        )
        activityIndicatorStatePublishRelay.accept(.show)
        networkService.loadDecodable(
            networkContext: networkContext,
            type: AuthUser.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
            switch result {
            case .success(let user):
                ClientSingleton.shared.currentUserRelay.accept(user)
                strongSelf.closeRelay.accept(())
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
}
