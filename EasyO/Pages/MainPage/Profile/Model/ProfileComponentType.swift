//
//  ProfileComponentType.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

enum ProfileComponentType {
    case role
    case email
    case logout
    
    var title: String? {
        switch self {
        case .email:
            return "Почта"
        case .role:
            return "Роль"
        case .logout:
            return nil
        }
    }
}
