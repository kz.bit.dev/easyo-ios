//
//  ProfileComponent.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct ProfileComponent {
    let type: ProfileComponentType
    let description: String?
}
