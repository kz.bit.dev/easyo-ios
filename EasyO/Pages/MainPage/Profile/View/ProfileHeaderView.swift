//
//  ProfileHeaderView.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol ProfileHeaderViewDelegate: class {
    func profileHeaderViewEditTapped()
}

class ProfileHeaderView: UIView {
    
    weak var delegate: ProfileHeaderViewDelegate?
    
    static let frame = CGRect(x: 0, y: 0, width: Constants.width, height: 120)
    
    private let profileImageView = CachedImageView()
    private let stackView = UIStackView()
    private let nameLabel = UILabel()
    private let editButton = UIButton(type: .system)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(name: String?) {
        nameLabel.text = name
    }
    
    func set(profileImageUrlString: String?) {
        profileImageView.loadImage(urlString: profileImageUrlString)
    }
    
    @objc private func editButtonTapped() {
        delegate?.profileHeaderViewEditTapped()
    }
}

extension ProfileHeaderView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(profileImageView)
        addSubview(stackView)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(editButton)
    }
    
    func setViewConstraints() {
        profileImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.bottom.equalToSuperview().offset(-30)
            make.width.height.equalTo(60)
        }
        
        stackView.snp.makeConstraints { make in
            make.leading.equalTo(profileImageView.snp.trailing).offset(24)
            make.centerY.equalTo(profileImageView.snp.centerY)
            make.trailing.equalToSuperview().offset(-24)
        }
    }
    
    func stylizeViews() {
        profileImageView.layer.cornerRadius = 30
        
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 8
        stackView.alignment = .fill
        
        nameLabel.font = .systemFont(ofSize: 22, weight: .medium)
        nameLabel.textColor = UIColor(hexString: "#334669")
        
        editButton.titleEdgeInsets.left = 12
        editButton.setImage(UIImage(named: "edit"), for: .normal)
        editButton.setTitle("Редактировать данные", for: .normal)
        editButton.setTitleColor(UIColor(hexString: "#00BFFF"), for: .normal)
        editButton.contentHorizontalAlignment = .left
        editButton.addTarget(self, action: #selector(editButtonTapped), for: .touchUpInside)
    }
}
