//
//  ProfileLogoutTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class ProfileLogoutTVCell: UITableViewCell, ReusableView {
    
    private let separatorView = UIView()
    private let iconImageView = UIImageView()
    private let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProfileLogoutTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(separatorView)
        addSubview(iconImageView)
        addSubview(titleLabel)
    }
    
    func setViewConstraints() {
        separatorView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        
        iconImageView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.top.equalToSuperview().offset(24)
            make.bottom.lessThanOrEqualToSuperview().offset(-24)
            make.width.height.equalTo(18)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(iconImageView.snp.trailing).offset(12)
            make.centerY.equalTo(iconImageView.snp.centerY)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        
        separatorView.backgroundColor = UIColor(hexString: "#C9D7E6").withAlphaComponent(0.7)
        
        iconImageView.image = UIImage(named: "logout")
        iconImageView.contentMode = .scaleAspectFill
        
        titleLabel.textColor = UIColor(hexString: "#FF647C")
        titleLabel.font = .systemFont(ofSize: 18, weight: .medium)
        titleLabel.text = "Выйти"
    }
}
