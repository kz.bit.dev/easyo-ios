//
//  ProfileViewController.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import SnapKit

class ProfileViewController: BaseRXTableViewController {
    
    private let viewModel = ProfileViewModel()
    private let headerView = ProfileHeaderView(frame: ProfileHeaderView.frame)
    private var components = [ProfileComponent]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        setupViewMoodel()
    }
    
    private func setupViewMoodel() {
        viewModel.componentsRelay.subscribe(onNext: { [unowned self] components in
            self.components = components
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.userRelay.subscribe(onNext: { [unowned self] user in
            self.headerView.set(name: user.firstName + " " + user.lastName)
            self.headerView.set(profileImageUrlString: user.imageUrl)
        }).disposed(by: disposeBag)
        
        viewModel.logoutRelay.subscribe(onNext: {
            guard let window = UIApplication.shared.windows.first else { return }
            LocalStore().removeAllValues()
            window.rootViewController = OnboardingViewController()
        }).disposed(by: disposeBag)
        
        viewModel.loadComponents()
    }
}

extension ProfileViewController: ProfileHeaderViewDelegate {
    func profileHeaderViewEditTapped() {
        let viewController = ProfileEditViewController()
        viewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension ProfileViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component = components[indexPath.row]
        switch component.type {
        case .logout:
            let cell: ProfileLogoutTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        case .email, .role:
            let cell: ProfileComponentTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.set(title: component.type.title)
            cell.set(description: component.description)
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let component = components[indexPath.row]
        switch component.type {
        case .logout:
            viewModel.logout()
        case .email:
            break
        case .role:
            break
        }
    }
}

extension ProfileViewController {
    private func logoutTapped() {
        
    }
    
    private func stylizeViews() {
        navigationItem.title = "Мой профиль"
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.separatorStyle = .none
        tableView.register(ProfileLogoutTVCell.self)
        tableView.register(ProfileComponentTVCell.self)
        tableView.tableHeaderView = headerView
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        headerView.delegate = self
    }
}
