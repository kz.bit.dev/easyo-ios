//
//  OrderModel.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct OrderModel: Codable, Hashable {
    let id: Int
    let orderedDate: String
    let table: ObjectId
    let acceptedUser: ObjectId
    let orderLineList: [OrderLineListItem]?
    let comment: String?
    
    func getTitle() -> String? {
        guard let tableNumber = ClientSingleton.shared.getTableNumber(tableId: table.id) else {
            return nil
        }
        return "Заказ №\(tableNumber) столика"
    }
    
    func getDescription() -> String? {
        guard let lines = orderLineList, !lines.isEmpty else { return nil }
        let meals = lines.compactMap { ClientSingleton.shared.getMealById(id: $0.meal.id) }.compactMap { $0.name }
        return meals.joined(separator: ", ")
    }
    
    func getOrderLineListWithPrepareStatus() -> [OrderLineListItem]? {
        guard let orderLineList = orderLineList else { return nil }
        var list = [OrderLineListItem]()
        for orderLine in orderLineList {
            guard orderLine.status == .prepare else { continue }
            let line = OrderLineListItem(
                id: orderLine.id,
                quantity: orderLine.quantity,
                unitPrice: orderLine.unitPrice,
                meal: orderLine.meal,
                status: orderLine.status,
                acceptedUser: orderLine.acceptedUser,
                tableNumber: nil
            )
            list.append(line)
        }
        return list
    }
    
    func getOrderLineListWithAwaitingStatus() -> [OrderLineListItem]? {
        guard let orderLineList = orderLineList else { return nil }
        var list = [OrderLineListItem]()
        for orderLine in orderLineList {
            guard orderLine.status != .awaiting else { continue }
            let line = OrderLineListItem(
                id: orderLine.id,
                quantity: orderLine.quantity,
                unitPrice: orderLine.unitPrice,
                meal: orderLine.meal,
                status: orderLine.status,
                acceptedUser: orderLine.acceptedUser,
                tableNumber: nil
            )
            list.append(line)
        }
        return list
    }
}
