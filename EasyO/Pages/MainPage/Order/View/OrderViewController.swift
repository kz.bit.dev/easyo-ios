//
//  OrderViewController.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class OrderViewController: BaseRXViewController {
    
    private let width = (Constants.width - 32 * (4 - 1) + 0.5) / 4
    
    private let viewModel = OrderViewModel()
    private var tables = [TableModel]()
    
    private let layout = VerticalCVLayout()
    private lazy var collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: layout
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        viewModel.myTablesRelay.subscribe(onNext: { [unowned self] tables in
            self.tables = tables
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadTables()
    }
}

extension OrderViewController {
    @objc private func notificationsTapped() {
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        switch user.userPositionEnum {
        case .cook:
            let viewController = CookNotificationsViewController()
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        default:
            let viewController = NotificationsViewController()
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension OrderViewController: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return tables.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: TableCVCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let table = tables[indexPath.row]
        cell.set(tableNumber: table.number)
        cell.set(tableStatus: ClientSingleton.shared.getTableStatus(tableId: table.id))
        return cell
    }
}

extension OrderViewController: UICollectionViewDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let viewController = OrderOfTableViewController(table: tables[indexPath.row])
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension OrderViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(collectionView)
    }
    
    func setViewConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
    }
    
    func stylizeViews() {
        navigationItem.title = "Столики"
        
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "notificationIcon")?.resized(to: CGSize(width: 19.5, height: 24)),
            style: .done,
            target: self,
            action: #selector(notificationsTapped)
        )
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        collectionView.contentInset.top = 16
        collectionView.contentInset.bottom = 16
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cell: TableCVCell.self)
        collectionView.showsVerticalScrollIndicator = false
    }
}

