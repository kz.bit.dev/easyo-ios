//
//  OrderViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class OrderViewModel: BaseViewModel {
    
    let myTablesRelay = PublishRelay<[TableModel]>()
    let ordersRelay = PublishRelay<[OrderModel]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var orders = [OrderModel]()
    private var tables = [TableModel]()
    
    override init() {
        super.init()
        subscribeClient()
        tables = ClientSingleton.shared.tablesRelay.value
        orders = ClientSingleton.shared.ordersRelay.value
    }
    
    func loadTables() {
        guard let currentUser: AuthUser = LocalStore().getValue(forKey: .currentUser) else {
            return
        }
        
        var filteredTables = [TableModel]()
        for table in tables {
            guard let order = table.order else { continue }
            guard currentUser.id == order.acceptedUser.id else { continue }
            filteredTables.append(table)
        }
        myTablesRelay.accept(filteredTables)
    }

    func subscribeClient() {
        ClientSingleton.shared.tablesRelay.subscribe(onNext: { [unowned self] tables in
            self.tables = tables
            self.loadTables()
        }).disposed(by: disposeBag)

        ClientSingleton.shared.ordersRelay.subscribe(onNext: { [unowned self] orders in
            self.orders = orders
            self.loadTables()
        }).disposed(by: disposeBag)
    }
}
