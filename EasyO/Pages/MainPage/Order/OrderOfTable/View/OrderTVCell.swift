//
//  MealTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class OrderTVCell: UITableViewCell, ReusableView {
    
    var indexPath: IndexPath?
    
    private let separatorView = UIView()
    private let menuImageView = CachedImageView()
    private let stackView = UIStackView()
    private let titleLabel = UILabel()
    private let priceLabel = UILabel()
    private let quantityLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(imageUrl: String?) {
        menuImageView.loadImage(urlString: imageUrl)
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
    
    func set(price: String?) {
        guard let price = price else { return }
        let attributedString = NSMutableAttributedString()
        attributedString.append(
            NSAttributedString(
                string: "Цена: ",
                attributes: [
                    .font: UIFont.systemFont(ofSize: 13),
                    .foregroundColor: UIColor.black
                ]
            )
        )
        attributedString.append(
            NSAttributedString(
                string: price,
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
                    NSAttributedString.Key.foregroundColor: UIColor(hexString: "#7A8AA3")
                ]
            )
        )
        priceLabel.attributedText = attributedString
    }
    
    func set(quantity: Int) {
        let attributedString = NSMutableAttributedString()
        attributedString.append(
            NSAttributedString(
                string: "Количество: ",
                attributes: [
                    .font: UIFont.systemFont(ofSize: 13),
                    .foregroundColor: UIColor.black
                ]
            )
        )
        attributedString.append(
            NSAttributedString(
                string: quantity.description,
                attributes: [
                    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
                    NSAttributedString.Key.foregroundColor: UIColor(hexString: "#7A8AA3")
                ]
            )
        )
        priceLabel.attributedText = attributedString
    }
}

extension OrderTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(separatorView)
        addSubview(menuImageView)
        addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(priceLabel)
        stackView.addArrangedSubview(quantityLabel)
    }
    
    func setViewConstraints() {
        separatorView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        
        menuImageView.snp.makeConstraints { make in
            make.leading.top.equalToSuperview().offset(24)
            make.bottom.lessThanOrEqualToSuperview().offset(-24)
            make.width.height.equalTo(80)
        }
        
        stackView.snp.makeConstraints { make in
            make.leading.equalTo(menuImageView.snp.trailing).offset(18)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-24)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        separatorView.backgroundColor = UIColor(hexString: "#C9D7E6").withAlphaComponent(0.7)
        
        menuImageView.contentMode = .scaleAspectFill
        menuImageView.layer.cornerRadius = 10
        
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.distribution = .fill
        stackView.alignment = .fill
        
        titleLabel.font = .systemFont(ofSize: 14, weight: .medium)
        titleLabel.numberOfLines = 2
        
        priceLabel.font = .systemFont(ofSize: 18, weight: .regular)
    }
}
