//
//  OrderOfTableViewController.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class OrderOfTableViewController: BaseRXTableViewController {
    
    private let viewModel: OrderOfTableViewModel
    private var orderLineList = [OrderLineListItem]()
    private let table: TableModel
    
    init(table: TableModel) {
        viewModel = OrderOfTableViewModel(table: table)
        self.table = table
        super.init(nibName: nil, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        
        viewModel.orderLineListRelay.subscribe(onNext: { [unowned self] orderLineList in
            self.orderLineList = orderLineList
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadOrders()
    }
}

extension OrderOfTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderLineList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: OrderTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        
        let orderLine = orderLineList[indexPath.row]
        if let meal = ClientSingleton.shared.getMealById(id: orderLine.meal.id) {
            cell.set(title: meal.name)
            cell.set(imageUrl: meal.imageUrl)
        }
        cell.set(quantity: orderLine.quantity)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension OrderOfTableViewController {
    private func stylizeViews() {
        navigationItem.title = "Столик №" + table.number
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.separatorStyle = .none
        tableView.register(OrderTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
