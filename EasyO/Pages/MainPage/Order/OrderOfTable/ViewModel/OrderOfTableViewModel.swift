//
//  OrderOfTableViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class OrderOfTableViewModel: BaseViewModel {
    
    let orderLineListRelay = PublishRelay<[OrderLineListItem]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private let table: TableModel
    
    init(table: TableModel) {
        self.table = table
        super.init()
    }
    
    func loadOrders() {
        guard let orderId = table.order?.id else { return }
        let networkContext = OrderByIdNetworkContext(id: orderId)
        activityIndicatorStatePublishRelay.accept(.show)
        networkService.loadDecodable(
            networkContext: networkContext,
            type: OrderModel.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
            switch result {
            case .success(let data):
                guard let orderLineList = data.orderLineList else { return }
                strongSelf.orderLineListRelay.accept(orderLineList)
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
}
