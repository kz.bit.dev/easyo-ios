//
//  OrderByIdNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class OrderByIdNetworkContext: NetworkContext {
    let route: Route
    let method: NetworkMethod = .get
    var parameters = [String: Any]()
    
    init(id: Int) {
        route = .ordersById(id: id)
    }
}
