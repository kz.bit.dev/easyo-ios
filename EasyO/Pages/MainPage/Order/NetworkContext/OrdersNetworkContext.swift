//
//  OrdersNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class OrdersNetworkContext: NetworkContext {
    
    let route: Route = .orders
    let method: NetworkMethod = .get
    var parameters = [String: Any]()
    let encoding: NetworkEncoding = .url
    
    init(size: Int = 1000) {
        parameters["size"] = size
    }
}
