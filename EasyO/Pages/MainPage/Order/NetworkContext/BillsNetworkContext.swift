//
//  BillsNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

class BillsNetworkContext: NetworkContext {
    
    let route: Route = .bills
    let method: NetworkMethod = .get
    let encoding: NetworkEncoding = .url
    var parameters = [String: Any]()
    
    init(size: Int = 1000) {
        parameters["size"] = size
    }
}

class BillsPostNetworkContext: NetworkContext {
    
    let route: Route = .bills
    let method: NetworkMethod = .post
    let encoding: NetworkEncoding = .json
    var parameters = [String: Any]()
    
    init(amount: Double, order: ObjectId, acceptedUser: ObjectId) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        parameters["billTime"] = formatter.string(from: Date())
        parameters["amount"] = amount
        parameters["order"] = order.getParameters()
        parameters["acceptedUser"] = acceptedUser.getParameters()
    }
}
