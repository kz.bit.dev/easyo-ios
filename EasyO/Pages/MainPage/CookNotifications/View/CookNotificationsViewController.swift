//
//  CookNotificationsViewController.swift
//  EasyO
//
//  Created by Аскар on 5/31/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class CookNotificationsViewController: BaseRXTableViewController {
    
    private let viewModel = CookNotificationsViewModel()
    
    private let headerTitleTypes = NotificationOwnType.allCases
    private var сomponents = [NotificationComponent]()
    
    override func loadView() {
        super.loadView()
        baseViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        
        viewModel.componentsRelay.subscribe(onNext: { [unowned self] components in
            self.сomponents = components
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadComponents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
}

extension CookNotificationsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return сomponents.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component = сomponents[indexPath.row]
        let cell: NotificationTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.addShadowForButton()
        cell.indexPath = indexPath
        cell.delegate = self
        cell.set(component: component)
        return cell
    }
}

extension CookNotificationsViewController: NotificationTVCellDelegate {
    func notificationTVCellAcceptTapped(indexPath: IndexPath) {
        viewModel.acceptTapped(indexPath: indexPath)
    }
}

extension CookNotificationsViewController {
    private func stylizeViews() {
        navigationItem.title = "Уведомления"
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.separatorStyle = .none
        tableView.register(NotificationTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
