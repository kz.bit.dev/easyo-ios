//
//  CookNotificationsViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/31/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class CookNotificationsViewModel: BaseViewModel {
    
    let componentsRelay = PublishRelay<[NotificationComponent]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private let builder = CookNotificationsComponentBuilder()
    private var components = [NotificationComponent]()
    
    func loadComponents() {
        
        ClientSingleton.shared.orderLinesRelay.subscribe(onNext: { [unowned self] orderLines in
            self.setComponents(orderLines: orderLines)
        }).disposed(by: disposeBag)
        
        setComponents(orderLines: ClientSingleton.shared.orderLinesRelay.value)
    }
    
    func setComponents(orderLines: [OrderLineListItem]) {
        components = builder.getComponentsFromOrders(orderLines: orderLines)
        componentsRelay.accept(components)
    }
    
    func acceptTapped(indexPath: IndexPath) {
        let component = components[indexPath.row]
        let dispatchGroup = DispatchGroup()
        activityIndicatorStatePublishRelay.accept(.show)
        for orderLine in component.orderLineList {
            let networkContext = getNetworkContext(orderLine: orderLine, orderId: component.orderId)
            dispatchGroup.enter()
            networkService.load(networkContext: networkContext) { response in
                dispatchGroup.leave()
                print(response.networkError as Any)
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.activityIndicatorStatePublishRelay.accept(.hide)
            print("done")
        }
    }
    
    func getNetworkContext(orderLine: OrderLineListItem, orderId: Int) -> NetworkContext {
        return OrderLineUpdateNetworkContext(
            id: orderLine.id,
            orderLine: orderLine,
            status: .awaiting,
            orderId: orderId
        )
    }
}
