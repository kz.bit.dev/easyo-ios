//
//  CookNotificationsComponentBuilder.swift
//  EasyO
//
//  Created by Аскар on 5/31/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class CookNotificationsComponentBuilder {
    
    func getComponentsFromOrders(orderLines: [OrderLineListItem]) -> [NotificationComponent] {
        var components = [NotificationComponent]()
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else {
            return components
        }
        var tableOrderLinesDictionary = [Int: [OrderLineListItem]]()
        
        for orderLine in orderLines {
            guard let tableId = ClientSingleton.shared.getTableId(
                tableNumber: orderLine.tableNumber
            ) else { continue }
            
            if var list = tableOrderLinesDictionary[tableId] {
                list.append(orderLine)
                tableOrderLinesDictionary[tableId] = list
            } else {
                tableOrderLinesDictionary[tableId] = [orderLine]
            }
        }
        
        for item in tableOrderLinesDictionary {
            guard !item.value.isEmpty else { continue }
            components.append(
                NotificationComponent(
                    acceptedUser: ObjectId(id: user.id),
                    table: ObjectId(id: item.key),
                    orderLineList: item.value,
                    orderId: item.value.first?.order?.id ?? 0
                )
            )
        }
        components.sort { n1, n2 -> Bool in return n1.table.id < n2.table.id }
        return components
    }
}
