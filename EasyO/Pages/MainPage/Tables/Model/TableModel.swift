//
//  TableModel.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct TableModel: Codable, Hashable {
    let id: Int
    let size: Int
    let number: String
    let reserved: Bool
    let order: OrderModel?
}

enum TableStatusType {
    case mine
    case empty
    case another
}
