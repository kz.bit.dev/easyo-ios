//
//  MealModel.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct MealModel: Codable, Hashable {
    let id: Int
    let name: String
    let description: String
    let unitPrice: Double
    let size: Double
    let imageUrl: String?
    
    func getPriceText() -> String? {
        return unitPrice.description + " тг" 
    }
}
