//
//  MealsNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class MealsNetworkContext: NetworkContext {
    let route: Route = .meals
    let method: NetworkMethod = .get
    var parameters = [String: Any]()
    
    init(menuId: Int, page: Int = 0, size: Int = 1000) {
        parameters["menuId"] = menuId
        parameters["page"] = page
        parameters["size"] = size
    }
}
