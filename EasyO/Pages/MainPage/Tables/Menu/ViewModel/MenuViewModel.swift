//
//  MenuViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class MenuViewModel: BaseViewModel {
    
    let menusRelay = PublishRelay<[MenuModel]>()
    let mealsRelay = PublishRelay<[MealModel]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var menus = [MenuModel]()
    
    func loadMenus() {
        menus = ClientSingleton.shared.menusRelay.value
        menusRelay.accept(menus)
    }
    
    func loadMeals(menu: MenuModel) {
        mealsRelay.accept(menu.mealList)
    }
}
