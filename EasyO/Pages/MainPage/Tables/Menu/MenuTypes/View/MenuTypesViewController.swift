//
//  MenuTypesViewController.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol MenuTypesViewControllerDelegate: class {
    func menuTypesViewControllerMenuSelected(menu: MenuModel)
}

class MenuTypesViewController: BaseRXTableViewController {
    
    weak var delegate: MenuTypesViewControllerDelegate?
    
    private let menus: [MenuModel]
    
    init(menus: [MenuModel]) {
        self.menus = menus
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
    }
}

extension MenuTypesViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuTypeTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let menu = menus[indexPath.row]
        cell.set(title: menu.menuType)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        navigationController?.dismiss(animated: true, completion: {
            self.delegate?.menuTypesViewControllerMenuSelected(
                menu: self.menus[indexPath.row]
            )
        })
    }
}

extension MenuTypesViewController {
    private func stylizeViews() {
        navigationItem.title = "Мой профиль"
        
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.separatorStyle = .none
        tableView.register(MenuTypeTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
