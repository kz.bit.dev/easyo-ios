//
//  MenuTypeTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuTypeTVCell: UITableViewCell, ReusableView {
    
    private let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
}

extension MenuTypeTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(titleLabel)
    }
    
    func setViewConstraints() {
        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.bottom.equalToSuperview().offset(-16)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        
        titleLabel.font = .systemFont(ofSize: 18, weight: .medium)
        titleLabel.textColor = UIColor(hexString: "#334669")
    }
}
