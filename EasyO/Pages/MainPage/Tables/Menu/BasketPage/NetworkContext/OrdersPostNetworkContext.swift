//
//  OrdersPostNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

class OrdersPostNetworkContext: NetworkContext {
    let route: Route = .orders
    let method: NetworkMethod = .post
    var parameters = [String: Any]()
    let encoding: NetworkEncoding = .json
    
    init(tableId: Int, acceptedUserId: Int, meals: [MealModel], comment: String?, status: OrderLineListItemStatusType) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        parameters["orderedDate"] = formatter.string(from: Date())
        parameters["table"] = ObjectId(id: tableId).getParameters()
        parameters["acceptedUser"] = ObjectId(id: acceptedUserId).getParameters()
        
        let meals = BasketSingleton.shared.getMeals()
        var mealsParameters = [[String: Any]]()
        for meal in meals {
            let item = OrderLineListItem(
                id: -1,
                quantity: BasketSingleton.shared.getAmount(meal: meal),
                unitPrice: meal.unitPrice,
                meal: ObjectId(id: meal.id),
                status: status,
                acceptedUser: ObjectId(id: acceptedUserId),
                tableNumber: nil
            )
            mealsParameters.append(item.getParameters())
        }
        parameters["orderLineList"] = mealsParameters
        parameters["comment"] = comment
        
        print(parameters.jsonStringRepresentation as Any)
    }
}
