//
//  OrderLinesPostNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class OrderLinesPostNetworkContext: NetworkContext {
    let route: Route = .orderLines
    let method: NetworkMethod = .post
    let encoding: NetworkEncoding = .json
    var parameters = [String: Any]()
    
    init(
        quantity: Int,
        unitPrice: Double,
        meal: ObjectId,
        order: ObjectId,
        acceptedUser: ObjectId,
        status: OrderLineListItemStatusType
    ) {
        parameters["quantity"] = quantity
        parameters["unitPrice"] = unitPrice
        parameters["meal"] = meal.getParameters()
        parameters["order"] = order.getParameters()
        parameters["acceptedUser"] = acceptedUser.getParameters()
        parameters["status"] = status.rawValue
    }
}
