//
//  OrderLinesNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 6/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class OrderLinesNetworkContext: NetworkContext {
    
    let route: Route = .orderLines
    let method: NetworkMethod = .get
    let encoding: NetworkEncoding = .url
    var parameters = [String: Any]()
    
    init(
        page: Int,
        orderLineStatusEnum: OrderLineListItemStatusType,
        size: Int = 1000
    ) {
        parameters["page"] = page
        parameters["orderLineStatusEnum"] = orderLineStatusEnum.rawValue
        parameters["size"] = size
    }
}
