//
//  OrderLineListItem.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

struct OrderLineListItem: Codable, Hashable {
    
    let id: Int
    let quantity: Int
    let unitPrice: Double
    let meal: ObjectId
    let status: OrderLineListItemStatusType
    let acceptedUser: ObjectId
    let tableNumber: String?
    var order: ObjectId?
    
    func getParameters() -> [String: Any] {
        var parameters = [String: Any]()
        
        parameters["quantity"] = quantity
        parameters["unitPrice"] = unitPrice
        parameters["meal"] = meal.getParameters()
        parameters["status"] = status.rawValue
        parameters["acceptedUser"] = acceptedUser.getParameters()
        
        return parameters
    }
}
