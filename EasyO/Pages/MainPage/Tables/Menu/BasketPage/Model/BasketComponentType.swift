//
//  BasketComponentType.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

enum BasketComponentType {
    case meal
    case button
    case comment
}
