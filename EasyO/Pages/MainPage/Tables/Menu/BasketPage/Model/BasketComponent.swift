//
//  BasketComponent.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct BasketComponent {
    let type: BasketComponentType
    var meal: MealModel?
}
