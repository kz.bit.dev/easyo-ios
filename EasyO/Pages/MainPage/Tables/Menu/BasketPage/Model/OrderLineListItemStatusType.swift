//
//  OrderLineListItemStatusType.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

enum OrderLineListItemStatusType: String, Codable, LosslessStringConvertible, CustomStringConvertible {
    
    case prepare = "PREPARE"
    case awaiting = "AWAITING"
    case done = "DONE"
    
    init?(_ description: String) {
        guard let data = description.data(using: .utf8) else { return nil }
        guard let item = try? JSONDecoder().decode(OrderLineListItemStatusType.self, from: data) else {
            return nil
        }
        self = item
    }

    var description: String {
        guard let jsonData = try? JSONEncoder().encode(self) else { return "" }
        let jsonString = String(data: jsonData, encoding: .utf8)
        return jsonString!
    }
}
