//
//  ButtonTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol ButtonTVCellDelegate: class {
    func buttonTVCellTapped(indexPath: IndexPath)
}

class ButtonTVCell: UITableViewCell, ReusableView {
    
    weak var delegate: ButtonTVCellDelegate?
    var indexPath: IndexPath?
    
    private let button = UIButton(type: .system)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String?) {
        button.setTitle(title, for: .normal)
    }
    
    func setShadow() {
        button.setAllSideShadow(shadowShowSize: 1)
    }
}

extension ButtonTVCell {
    @objc private func buttonTapped() {
        guard let indexPath = indexPath else { return }
        delegate?.buttonTVCellTapped(indexPath: indexPath)
    }
}

extension ButtonTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(button)
    }
    
    func setViewConstraints() {
        button.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(4)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.bottom.lessThanOrEqualToSuperview().offset(-4)
            make.height.equalTo(52)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        button.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        button.backgroundColor = UIColor(hexString: "#E8F3FB")
    }
}
