//
//  BasketViewController.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class BasketViewController: BaseRXTableViewController {
    
    private let viewModel: BasketViewModel
    private var components = [BasketComponent]()
    
    private var activeCell: UITableViewCell?
    private var keyboardObservers = [Any]()
    
    init(tableId: Int) {
        viewModel = BasketViewModel(tableId: tableId)
        super.init(nibName: nil, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        setupViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        set(keyboardObservers: &keyboardObservers)
        tableView.reloadData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        remove(keyboardObservers: &keyboardObservers)
    }
    
    private func setupViewModel() {
        viewModel.componentsRelay.subscribe(onNext: { [unowned self] components in
            self.components = components
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.closeRelay.subscribe(onNext: { [unowned self] in
            self.navigationController?.popToRootViewController(animated: true)
        }).disposed(by: disposeBag)
        
        viewModel.loadComponents()
    }
}

extension BasketViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return components.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component = components[indexPath.row]
        switch component.type {
        case .meal:
            let cell: MealTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            let meal = component.meal!
            cell.set(title: meal.name)
            cell.set(imageUrl: meal.imageUrl)
            cell.indexPath = indexPath
            cell.delegate = self
            cell.set(value: BasketSingleton.shared.getAmount(meal: meal))
            return cell
        case .comment:
            let cell: CommentTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.set(cornerRadius: 20)
            cell.indexPath = indexPath
            cell.delegate = self
            return cell
        case .button:
            let cell: ButtonTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.delegate = self
            cell.indexPath = indexPath
            cell.set(title: "Подтвердить заказ")
            cell.setShadow()
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension BasketViewController: ButtonTVCellDelegate {
    func buttonTVCellTapped(indexPath: IndexPath) {
        viewModel.orderTapped()
    }
}

extension BasketViewController: MealTVCellDelegate {
    func mealTVCellStepperValueChanged(value: Int, indexPath: IndexPath) {
        guard let meal = components[indexPath.row].meal else { return }
        BasketSingleton.shared.mealAmountChanged(meal: meal, amount: value)
    }
}

extension BasketViewController: CommentTVCellDelegate {
    func commentTVCellTextViewDidBeginEditing(indexPath: IndexPath) {
        activeCell = tableView.cellForRow(at: indexPath)
    }
    
    func commentTVCellTextViewDidChange(indexPath: IndexPath, text: String) {
        viewModel.set(comment: text)
    }
}

extension BasketViewController: KeyboardCoverProtocol {
    var targetFrame: CGRect? {
        guard let activeCell = activeCell else { return nil }
        return tableView.convert(activeCell.frame, to: view)
    }
    
    var scrollableView: UIScrollView {
        return tableView
    }
}

extension BasketViewController {
    private func stylizeViews() {
        navigationItem.title = "Корзина"
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.register(MealTVCell.self)
        tableView.register(CommentTVCell.self)
        tableView.register(ButtonTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
    }
}
