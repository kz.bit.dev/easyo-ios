//
//  CommentTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/8/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol CommentTVCellDelegate: class {
    func commentTVCellTextViewDidBeginEditing(indexPath: IndexPath)
    func commentTVCellTextViewDidChange(indexPath: IndexPath, text: String)
}

class CommentTVCell: UITableViewCell, ReusableView {
    
    weak var delegate: CommentTVCellDelegate?
    var indexPath: IndexPath?
    
    private let textView = UITextView()
    private let strokeView = GradientView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(cornerRadius: CGFloat) {
        strokeView.cornerRadius = cornerRadius
        textView.layer.cornerRadius = cornerRadius
        
        strokeView.startColor = UIColor.white
        strokeView.endColor = UIColor(hexString: "#CCD9E4")
        strokeView.cornerRadius = 15
        strokeView.endPointX = 1
        strokeView.endPointY = 1
    }
}

extension CommentTVCell: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        guard let indexPath = indexPath else { return }
        delegate?.commentTVCellTextViewDidBeginEditing(indexPath: indexPath)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        guard let indexPath = indexPath else { return }
        delegate?.commentTVCellTextViewDidChange(indexPath: indexPath, text: textView.text)
    }
}

extension CommentTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(strokeView)
        addSubview(textView)
    }
    
    func setViewConstraints() {
        strokeView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.top.equalToSuperview().offset(20)
            make.bottom.lessThanOrEqualToSuperview().offset(-8)
        }
        
        textView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(28)
            make.trailing.equalToSuperview().offset(-28)
            make.top.equalToSuperview().offset(24)
            make.bottom.lessThanOrEqualToSuperview().offset(-12)
            make.height.equalTo(100)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        clipsToBounds = false
        
        strokeView.startColor = UIColor.white
        strokeView.endColor = UIColor(hexString: "#CCD9E4")
        strokeView.cornerRadius = 15
        strokeView.endPointX = 1
        strokeView.endPointY = 1
        
        textView.font = .systemFont(ofSize: 14, weight: .regular)
        textView.textContainerInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        textView.layer.cornerRadius = 15
        textView.autocorrectionType = .no
        textView.autocapitalizationType = .none
        textView.backgroundColor = UIColor(hexString: "#DEEBF6")
        textView.delegate = self
    }
}
