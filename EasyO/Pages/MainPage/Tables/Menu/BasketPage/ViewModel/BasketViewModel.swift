//
//  BasketViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class BasketViewModel: BaseViewModel {
    
    let componentsRelay = PublishRelay<[BasketComponent]>()
    let closeRelay = PublishRelay<Void>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var comment: String?
    private let tableId: Int
    
    init(tableId: Int) {
        self.tableId = tableId
        super.init()
    }
    
    func loadComponents() {
        let meals = BasketSingleton.shared.getMeals()
        var components = [BasketComponent]()
        guard !meals.isEmpty else {
            return
        }
        for meal in meals {
            let component = BasketComponent(type: .meal, meal: meal)
            components.append(component)
        }
        components.append(.init(type: .comment, meal: nil))
        components.append(.init(type: .button, meal: nil))
        componentsRelay.accept(components)
    }
    
    func set(comment: String?) {
        if comment?.isEmpty ?? false {
            self.comment = nil
        } else {
            self.comment = comment
        }
    }
    
    func orderTapped() {
        guard let currentUser: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        let tables = ClientSingleton.shared.tablesRelay.value
        if let current = tables.first(where: { $0.id == tableId }),
           let order = current.order {
            
            let meals = BasketSingleton.shared.getMeals()
            let dispatchGroup = DispatchGroup()
            for meal in meals {
                let networkContext = OrderLinesPostNetworkContext(
                    quantity: BasketSingleton.shared.getAmount(meal: meal),
                    unitPrice: meal.unitPrice,
                    meal: ObjectId(id: meal.id),
                    order: ObjectId(id: order.id),
                    acceptedUser: ObjectId(id: currentUser.id),
                    status: .prepare
                )
                dispatchGroup.enter()
                networkService.loadDecodable(
                    networkContext: networkContext,
                    type: ObjectId.self
                ) { [weak self] result in
                    guard let strongSelf = self else { return }
                    dispatchGroup.leave()
                    switch result {
                    case .success:
                        print("success")
                    case .error(let error):
                        strongSelf.errorDisplayPublishRelay.accept(error)
                    }
                }
            }
            dispatchGroup.notify(queue: .main) {
                BasketSingleton.shared.clear()
                self.closeRelay.accept(())
            }
        } else {
            let networkContext = OrdersPostNetworkContext(
                tableId: tableId,
                acceptedUserId: currentUser.id,
                meals: BasketSingleton.shared.getMeals(),
                comment: comment,
                status: .prepare
            )
            activityIndicatorStatePublishRelay.accept(.show)
            networkService.loadDecodable(
                networkContext: networkContext,
                type: ObjectId.self
            ) { [weak self] result in
                guard let strongSelf = self else { return }
                strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
                switch result {
                case .success:
                    BasketSingleton.shared.clear()
                    strongSelf.closeRelay.accept(())
                case .error(let error):
                    strongSelf.errorDisplayPublishRelay.accept(error)
                }
            }
        }
    }
}
