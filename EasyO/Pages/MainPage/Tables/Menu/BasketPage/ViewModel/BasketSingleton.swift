//
//  BasketSingleton.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class BasketSingleton {
    
    static let shared = BasketSingleton()
    
    private var dictionary = [MealModel: Int]()
    
    private init() {}
    
    func mealAmountChanged(meal: MealModel, amount: Int) {
        if amount <= 0 {
            dictionary[meal] = nil
        } else {
            dictionary[meal] = amount
        }
    }
    
    func getAmount(meal: MealModel) -> Int {
        return dictionary[meal] ?? 0
    }
    
    func clear() {
        dictionary.removeAll()
    }
    
    func getMeals() -> [MealModel] {
        return dictionary.compactMap { $0.key }
    }
}
