//
//  MenuViewController.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuViewController: BaseRXCustomTableViewController {
    
    private let viewModel = MenuViewModel()
    
    private let headerView = MenuTypeView(frame: MenuTypeView.frame)
    private let basketButton = UIButton(type: .system)
    private var menus = [MenuModel]()
    private var meals = [MealModel]()
    private var currentMenu: MenuModel? {
        didSet {
            guard let menu = currentMenu else { return }
            headerView.set(title: menu.menuType)
            viewModel.loadMeals(menu: menu)
        }
    }
    private let tableId: Int
    
    init(tableId: Int) {
        self.tableId = tableId
        super.init(nibName: nil, bundle: nil)
        BasketSingleton.shared.clear()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubviews()
        setViewConstraints()
        stylizeViews()
        setupViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        headerView.containerView.setAllSideShadow(shadowShowSize: 1)
        basketButton.setAllSideShadow(shadowShowSize: 1)
        tableView.reloadData()
    }
    
    private func setupViewModel() {
        viewModel.menusRelay.subscribe(onNext: { [unowned self] menus in
            self.menus = menus
            self.currentMenu = menus.first
        }).disposed(by: disposeBag)
        
        viewModel.mealsRelay.subscribe(onNext: { [unowned self] meals in
            self.meals = meals
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadMenus()
    }
}

extension MenuViewController {
    @objc private func headerViewTapped() {
        let viewController = MenuTypesViewController(menus: menus)
        viewController.delegate = self
        present(UINavigationController(rootViewController: viewController), animated: true)
    }
    
    @objc private func basketTapped() {
        let viewController = BasketViewController(tableId: tableId)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MealTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let meal = meals[indexPath.row]
        cell.set(title: meal.name)
        cell.set(imageUrl: meal.imageUrl)
        cell.indexPath = indexPath
        cell.delegate = self
        cell.set(value: BasketSingleton.shared.getAmount(meal: meal))
        cell.set(price: meal.getPriceText())
        return cell
    }
}

extension MenuViewController: UITableViewDelegate {}

extension MenuViewController: MealTVCellDelegate {
    func mealTVCellStepperValueChanged(value: Int, indexPath: IndexPath) {
        BasketSingleton.shared.mealAmountChanged(meal: meals[indexPath.row], amount: value)
    }
}

extension MenuViewController: MenuTypesViewControllerDelegate {
    func menuTypesViewControllerMenuSelected(menu: MenuModel) {
        self.currentMenu = menu
    }
}

extension MenuViewController {
    private func addSubviews() {
        view.addSubview(basketButton)
    }
    
    private func setViewConstraints() {
        basketButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo(52)
        }
        basketButton.translatesAutoresizingMaskIntoConstraints = false
        basketButton.bottomAnchor.constraint(
            equalTo: view.safeAreaLayoutGuide.bottomAnchor,
            constant: -24
        ).isActive = true
    }
    
    private func stylizeViews() {
        navigationItem.title = "Новый заказ"
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.register(MealTVCell.self)
        tableView.tableHeaderView = headerView
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset.bottom = view.safeAreaInsets.bottom + 52 + 24 + 24
        
        headerView.isUserInteractionEnabled = true
        headerView.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(headerViewTapped))
        )
        
        basketButton.setTitleColor(.black, for: .normal)
        basketButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        basketButton.setTitle("Корзина", for: .normal)
        basketButton.addTarget(self, action: #selector(basketTapped), for: .touchUpInside)
    }
}
