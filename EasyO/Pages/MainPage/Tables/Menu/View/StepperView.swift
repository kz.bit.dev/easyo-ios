//
//  StepperView.swift
//  EasyO
//
//  Created by Аскар on 5/3/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol StepperViewDelegate: class {
    func stepperViewAmountChanged(amount: Int)
}

class StepperView: UIView {
    
    let width: CGFloat = 40
    let height: CGFloat = 24 + 10 + 32 + 10 + 24
    
    weak var delegate: StepperViewDelegate?
    
    private var amount: Int = 0
    
    private let containerView = UIView()
    private let minusButton = UIImageView()
    private let plusButton = UIImageView()
    private let amountLabel = MaskedRoundedTextField()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(amount: Int) {
        self.amount = amount
        amountLabel.text = amount.description
    }
}

extension StepperView {
    @objc private func plusButtonTapped() {
        guard amount < 99 else { return }
        amount = amount + 1
        set(amount: amount)
        delegate?.stepperViewAmountChanged(amount: amount)
    }
    
    @objc private func minusButtonTapped() {
        guard amount > 0 else { return }
        amount = amount - 1
        set(amount: amount)
        delegate?.stepperViewAmountChanged(amount: amount)
    }
}

extension StepperView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(plusButton)
        containerView.addSubview(amountLabel)
        containerView.addSubview(minusButton)
    }
    
    func setViewConstraints() {
        containerView.snp.makeConstraints { make in
            make.leading.top.trailing.bottom.equalToSuperview()
        }
        
        plusButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.height.equalTo(24)
            make.top.equalToSuperview()
        }
        
        amountLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.equalTo(32)
            make.width.equalTo(40)
        }
        
        minusButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.height.equalTo(24)
            make.bottom.equalToSuperview()
        }
    }
    
    func stylizeViews() {
        plusButton.image = UIImage(named: "plusButton")
        plusButton.isUserInteractionEnabled = true
        plusButton.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(plusButtonTapped))
        )
        plusButton.backgroundColor = .clear
        
        amountLabel.textfield.tintColor = .black
        amountLabel.textfield.textAlignment = .center
        amountLabel.textfield.font = .systemFont(ofSize: 14)
        amountLabel.textfield.textColor = .black
        amountLabel.textfield.layer.borderColor = UIColor.clear.cgColor
        amountLabel.textfield.layer.borderWidth = 0
        amountLabel.textfield.keyboardType = .default
        amountLabel.textfield.isUserInteractionEnabled = false
        amountLabel.text = amount.description
        amountLabel.set(cornerRadius: 8)
        
        minusButton.image = UIImage(named: "minusButton")
        minusButton.isUserInteractionEnabled = true
        minusButton.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(minusButtonTapped))
        )
        minusButton.backgroundColor = .clear
    }
}
