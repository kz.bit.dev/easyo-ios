//
//  MenuTypeView.swift
//  EasyO
//
//  Created by Аскар on 5/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuTypeView: UIView {
    
    static let frame = CGRect(x: 0, y: 0, width: Constants.width, height: 92)
    
    let containerView = UIView()
    private let titleLabel = UILabel()
    private let arrowImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
}

extension MenuTypeView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(containerView)
        addSubview(titleLabel)
        addSubview(arrowImageView)
    }
    
    func setViewConstraints() {
        containerView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo(52)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(containerView.snp.leading).offset(20)
            make.centerY.equalTo(containerView.snp.centerY)
        }
        
        arrowImageView.snp.makeConstraints { make in
            make.trailing.equalTo(containerView.snp.trailing).offset(-20)
            make.centerY.equalTo(containerView.snp.centerY)
            make.width.equalTo(14)
            make.height.equalTo(16)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        
        containerView.backgroundColor = UIColor(hexString: "#E8F3FB")
        
        titleLabel.textColor = UIColor(hexString: "#334669")
        titleLabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        
        arrowImageView.image = UIImage(named: "arrowBottom")
        arrowImageView.contentMode = .scaleAspectFit
    }
}
