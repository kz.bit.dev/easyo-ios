//
//  TablePayView.swift
//  EasyO
//
//  Created by Аскар on 5/9/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol TablePayViewDelegate: class {
    func tablePayViewPayTapped(tableNumber: String)
    func tablePayViewAddOrderTapped(tableNumber: String)
}

class TablePayView: UIView {
    
    static let frame = CGRect(x: 0, y: 0, width: Constants.width, height: Constants.heigth)
    
    weak var delegate: TablePayViewDelegate?
    
    private let background = UIView()
    private let containerView = UIView()
    private let closeButton = UIButton(type: .system)
    private let verticalStackView = UIStackView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let payButton = UIButton(type: .system)
    private let addOrderButton = UIButton(type: .system)
    
    private let tableNumber: String
    
    init(tableNumber: String) {
        self.tableNumber = tableNumber
        super.init(frame: TablePayView.frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        payButton.setAllSideShadow(shadowShowSize: 1)
    }
    
    @objc private func closeTapped() {
        removeFromSuperview()
    }
    
    @objc private func payButtonTapped() {
        delegate?.tablePayViewPayTapped(tableNumber: tableNumber)
        removeFromSuperview()
    }
    
    @objc private func addOrdetButtonTapped() {
        delegate?.tablePayViewAddOrderTapped(tableNumber: tableNumber)
        removeFromSuperview()
    }
}

extension TablePayView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(background)
        addSubview(containerView)
        addSubview(closeButton)
        containerView.addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(SpacerView(space: 48))
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.addArrangedSubview(SpacerView(space: 16))
        verticalStackView.addArrangedSubview(descriptionLabel)
        verticalStackView.addArrangedSubview(SpacerView(space: 32))
        verticalStackView.addArrangedSubview(payButton)
        verticalStackView.addArrangedSubview(SpacerView(space: 24))
        verticalStackView.addArrangedSubview(addOrderButton)
        verticalStackView.addArrangedSubview(SpacerView(space: 32))
    }
    
    func setViewConstraints() {
        background.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        
        containerView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.centerY.equalToSuperview()
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.lessThanOrEqualToSuperview()
        }
        
        closeButton.snp.makeConstraints { make in
            make.top.equalTo(containerView.snp.top).offset(18)
            make.trailing.equalTo(containerView.snp.trailing).offset(-18)
            make.width.height.equalTo(26)
        }
        
        payButton.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(52)
        }
        
        addOrderButton.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalTo(24)
        }
    }
    
    func stylizeViews() {
        backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        background.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeTapped)))
        
        containerView.backgroundColor = UIColor(hexString: "#E4F0FA")
        containerView.layer.cornerRadius = 16
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fill
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 0
        
        closeButton.addTarget(self, action: #selector(closeTapped), for: .touchUpInside)
        closeButton.setImage(UIImage(named: "close"), for: .normal)
        
        titleLabel.text = "Столик №" + tableNumber.description
        titleLabel.font = .systemFont(ofSize: 20, weight: .regular)
        titleLabel.textColor = .black
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 0
        
        descriptionLabel.text = "Вы можете закрыть или же добавить заказ"
        descriptionLabel.font = .systemFont(ofSize: 14, weight: .regular)
        descriptionLabel.textColor = UIColor(hexString: "#7A8AA3").withAlphaComponent(0.7)
        descriptionLabel.textAlignment = .center
        descriptionLabel.numberOfLines = 0
        
        payButton.setTitle("Оплата", for: .normal)
        payButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        payButton.layer.cornerRadius = 12
        payButton.addTarget(self, action: #selector(payButtonTapped), for: .touchUpInside)
        
        addOrderButton.setTitle("Добавить заказ", for: .normal)
        addOrderButton.addTarget(self, action: #selector(addOrdetButtonTapped), for: .touchUpInside)
    }
}
