//
//  TablesViewController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TablesViewController: BaseRXViewController {
    
    private let width = (Constants.width - 32 * (4 - 1) + 0.5) / 4
    
    private let viewModel = TablesViewModel()
    private var tables = [TableModel]()
    
    private let layout = VerticalCVLayout()
    private lazy var collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: layout
    )
    
    private let socketService = SocketService()
    
    override func loadView() {
        super.loadView()
        baseViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        ClientSingleton.shared.tablesRelay.subscribe(onNext: { [unowned self] tables in
            self.tables = tables
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
        
        ClientSingleton.shared.ordersRelay.subscribe(onNext: { [unowned self] _ in
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.successBillRelay.subscribe(onNext: { [unowned self] text in
            self.showSuccess(message: text, completion: nil)
        }).disposed(by: disposeBag)
        
        viewModel.loadTables()
    }
}

extension TablesViewController {
    @objc private func notificationsTapped() {
        let viewController = NotificationsViewController()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc private func reloadTapped() {
        if let token: String = LocalStore().getValue(forKey: .token) {
            socketService.openConnection(token: token)
            ClientSingleton.shared.loadData(completion: { _ in })
        }
    }
}

extension TablesViewController: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return tables.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == tables.count {
            let cell: TableTypeInformationTVCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
            return cell
        }
        let cell: TableCVCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let table = tables[indexPath.row]
        cell.set(tableNumber: table.number)
        cell.set(tableStatus: ClientSingleton.shared.getTableStatus(tableId: table.id))
        return cell
    }
}

extension TablesViewController: UICollectionViewDelegate {
    func collectionView(
        _ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath
    ) {
        collectionView.deselectItem(at: indexPath, animated: true)
        guard indexPath.row != tables.count else {
            let viewController = TableTypeDescriptionViewController()
            showViewControllerWithStorkTransitionDelegate(viewController: viewController, height: 300)
            return
        }
        let table = tables[indexPath.row]
        let status = ClientSingleton.shared.getTableStatus(tableId: table.id)
        switch status {
        case .mine, .another:
            let payView = TablePayView(tableNumber: table.number)
            payView.delegate = self
            tabBarController?.view.addSubview(payView)
        case .empty:
            let viewController = MenuViewController(tableId: tables[indexPath.row].id)
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension TablesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        if indexPath.row == tables.count {
            return .init(width: Constants.width, height: 40)
        }
        return .init(width: width, height: width)
    }
}

extension TablesViewController: TablePayViewDelegate {
    func tablePayViewPayTapped(tableNumber: String) {
        guard let orderId = viewModel.getOrderIdBy(tableNumber: tableNumber) else { return }
        viewModel.getPaySum(orderId: orderId) { [weak self] price in
            let viewController = TablePayTotalViewController(orderId: orderId, price: price)
            viewController.delegate = self
            self?.showViewControllerWithStorkTransitionDelegate(viewController: viewController, height: 300)
        }
    }
    
    func tablePayViewAddOrderTapped(tableNumber: String) {
        guard let table = tables.first(where: { $0.number == tableNumber }) else { return }
        let viewController = MenuViewController(tableId: table.id)
        viewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension TablesViewController: TablePayTotalViewControllerDelegate {
    func tablePayTotalViewControllerShowSuccess(text: String) {
        showSuccess(message: text, completion: nil)
    }
    
    func tablePayTotalViewControllerShowError(error: AppError) {
        showError(error: error)
    }
}

extension TablesViewController: ViewInstallationProtocol {
    private func showViewControllerWithStorkTransitionDelegate(viewController: UIViewController, height: CGFloat) {
        let transitionDelegate = SPStorkTransitioningDelegate()
        var customHeight = height
        if #available(iOS 11.0, *) {
            customHeight += view.safeAreaInsets.bottom
        }
        transitionDelegate.customHeight = min(customHeight, 432)
        
        viewController.transitioningDelegate = transitionDelegate
        viewController.modalPresentationStyle = .custom
        self.present(viewController, animated: true, completion: nil)
    }
    
    func addSubviews() {
        view.addSubview(collectionView)
    }
    
    func setViewConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
        }
    }
    
    func stylizeViews() {
        navigationItem.title = "Столики"
        
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "notificationIcon")?.resized(to: CGSize(width: 19.5, height: 24)),
            style: .done,
            target: self,
            action: #selector(notificationsTapped)
        )
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            title: "Обновить",
            style: .done,
            target: self,
            action: #selector(reloadTapped)
        )
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        collectionView.contentInset.top = 16
        collectionView.contentInset.bottom = 16
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cell: TableCVCell.self)
        collectionView.register(cell: TableTypeInformationTVCell.self)
        collectionView.showsVerticalScrollIndicator = false
    }
}
