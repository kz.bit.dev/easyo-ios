//
//  TableTypeInformationTVCell.swift
//  EasyO
//
//  Created by Аскар on 6/5/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TableTypeInformationTVCell: UICollectionViewCell, ReusableView {
    
    private let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension TableTypeInformationTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(titleLabel)
    }
    
    func setViewConstraints() {
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
    }
    
    func stylizeViews() {
        titleLabel.text = "Информация о столиках"
        titleLabel.textColor = UIColor(hexString: "#00BFFF")
        titleLabel.font = .systemFont(ofSize: 18, weight: .regular)
    }
}
