//
//  TableCVCell.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TableCVCell: UICollectionViewCell, ReusableView {
    
    private let containerView = UIView()
    private let numberLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(tableNumber: String?) {
        numberLabel.text = tableNumber
    }
    
    func set(tableStatus: TableStatusType) {
        switch tableStatus {
        case .another:
            containerView.layer.borderColor = UIColor.red.cgColor
            numberLabel.textColor = .red
        case .mine:
            containerView.layer.borderColor = UIColor.green.cgColor
            numberLabel.textColor = .green
        case .empty:
            let color = UIColor(hexString: "#B1C5D5").withAlphaComponent(0.4)
            containerView.layer.borderColor = color.cgColor
            numberLabel.textColor = color
        }
    }
}

extension TableCVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(containerView)
        containerView.addSubview(numberLabel)
    }
    
    func setViewConstraints() {
        containerView.snp.makeConstraints { make in
            make.leading.top.trailing.bottom.equalToSuperview()
        }
        
        numberLabel.snp.makeConstraints { make in
            make.centerX.centerY.equalToSuperview()
        }
    }
    
    func stylizeViews() {
        containerView.layer.borderWidth = 1.2
        containerView.layer.borderColor = UIColor(hexString: "#B1C5D5").withAlphaComponent(0.4).cgColor
        containerView.layer.cornerRadius = 12
        
        numberLabel.textColor = UIColor(hexString: "#B1C5D5").withAlphaComponent(0.4)
        numberLabel.font = .systemFont(ofSize: 18)
    }
}
