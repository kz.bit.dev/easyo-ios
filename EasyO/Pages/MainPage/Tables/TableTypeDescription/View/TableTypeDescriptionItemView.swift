//
//  TableTypeDescriptionItemView.swift
//  EasyO
//
//  Created by Аскар on 6/5/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TableTypeDescriptionItemView: UIView {
    
    private let colorView = UIView()
    private let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(color: UIColor?) {
        colorView.backgroundColor = color
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
}

extension TableTypeDescriptionItemView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(colorView)
        addSubview(titleLabel)
    }
    
    func setViewConstraints() {
        colorView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(11)
            make.leading.equalToSuperview().offset(24)
            make.bottom.lessThanOrEqualToSuperview().offset(-11)
            make.width.height.equalTo(16)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(colorView.snp.trailing).offset(14)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-24)
        }
    }
    
    func stylizeViews() {
        colorView.layer.cornerRadius = 2
        colorView.backgroundColor = UIColor(hexString: "#C8D4E2")
        
        titleLabel.font = .systemFont(ofSize: 16, weight: .regular)
    }
}
