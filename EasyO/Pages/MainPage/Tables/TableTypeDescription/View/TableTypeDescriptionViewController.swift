//
//  TableTypeDescriptionViewController.swift
//  EasyO
//
//  Created by Аскар on 6/5/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TableTypeDescriptionViewController: BaseRXViewController {
    
    private let topSeparatorView = UIView()
    private let verticalStackView = UIStackView()
    private let emptyTablesView = TableTypeDescriptionItemView()
    private let otherTablesView = TableTypeDescriptionItemView()
    private let myTablesView = TableTypeDescriptionItemView()
    private let okButton = UIButton(type: .system)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        okButton.setAllSideShadow(shadowShowSize: 1)
    }
}

extension TableTypeDescriptionViewController {
    @objc private func okButtonTapped() {
        dismiss(animated: true)
    }
}

extension TableTypeDescriptionViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(topSeparatorView)
        view.addSubview(verticalStackView)
        view.addSubview(okButton)
        verticalStackView.addArrangedSubview(emptyTablesView)
        verticalStackView.addArrangedSubview(otherTablesView)
        verticalStackView.addArrangedSubview(myTablesView)
    }
    
    func setViewConstraints() {
        topSeparatorView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(120)
            make.height.equalTo(3)
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(topSeparatorView.snp.bottom).offset(40)
        }
        
        okButton.snp.makeConstraints { make in
            make.top.equalTo(verticalStackView.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
            make.height.equalTo(52)
            make.width.equalTo(view.snp.width).offset(-48)
        }
    }
    
    func stylizeViews() {
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        topSeparatorView.layer.cornerRadius = 1.5
        topSeparatorView.backgroundColor = UIColor(rgb: 110, 129, 160, alpha: 23)
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fill
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 0
        
        emptyTablesView.set(color: UIColor(hexString: "#C8D4E2"))
        emptyTablesView.set(title: "Свободные столики")
        
        otherTablesView.set(color: UIColor(hexString: "#FF647C"))
        otherTablesView.set(title: "Столики других официантов")
        
        myTablesView.set(color: UIColor(hexString: "#29CB97"))
        myTablesView.set(title: "Мои столики")
        
        okButton.setTitle("Понятно!", for: .normal)
        okButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        okButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
    }
}
