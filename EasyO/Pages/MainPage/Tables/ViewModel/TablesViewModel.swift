//
//  TablesViewModel.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class TablesViewModel: BaseViewModel {
    
    let tablesRelay = PublishRelay<[TableModel]>()
    let successBillRelay = PublishRelay<String>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var tables = [TableModel]()
    
    func loadTables() {
        tables = ClientSingleton.shared.tablesRelay.value
        tablesRelay.accept(tables)
        
        ClientSingleton.shared.tablesRelay.subscribe(onNext: { [unowned self] tables in
            self.tables = tables
            self.tablesRelay.accept(tables)
        }).disposed(by: disposeBag)
    }
    
    func getOrderIdBy(tableNumber: String) -> Int? {
        guard let order = tables.first(where: { $0.number == tableNumber })?.order else { return nil }
        return order.id
    }
    
    func getPaySum(orderId: Int, completion: @escaping (Double) -> Void) {
        let networkContext = OrdersPriceByIdNetworkContext(orderId: orderId)
        networkService.loadDecodable(
            networkContext: networkContext,
            type: ObjectPrice.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let data):
                completion(data.price.roundTo(places: 2))
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
}

struct ObjectPrice: Codable {
    let price: Double
}

extension Double {
    func roundTo(places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
