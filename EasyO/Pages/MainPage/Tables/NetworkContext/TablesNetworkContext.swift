//
//  TablesNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class TablesNetworkContext: NetworkContext {
    
    let route: Route = .tables
    let method: NetworkMethod = .get
    var parameters = [String: Any]()

    init(page: Int = 0, size: Int = 1000) {
        parameters["page"] = page
        parameters["size"] = size
        parameters["billed"] = false
    }
}
