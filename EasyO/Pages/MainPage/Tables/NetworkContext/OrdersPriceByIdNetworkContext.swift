//
//  OrdersPriceByIdNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 6/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class OrdersPriceByIdNetworkContext: NetworkContext {
    
    let route: Route
    let method: NetworkMethod = .get
    var parameters = [String: Any]()
    
    init(orderId: Int) {
        route = .ordersPriceById(id: orderId)
    }
}
