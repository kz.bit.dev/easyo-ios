//
//  TablePayTotalViewController.swift
//  EasyO
//
//  Created by Аскар on 6/5/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol TablePayTotalViewControllerDelegate: class {
    func tablePayTotalViewControllerShowSuccess(text: String)
    func tablePayTotalViewControllerShowError(error: AppError)
}

class TablePayTotalViewController: BaseRXViewController {
    
    weak var delegate: TablePayTotalViewControllerDelegate?
    
    private let viewModel: TablePayTotalViewModel
    
    private let topSeparatorView = UIView()
    private let verticalStackView = UIStackView()
    private let orderSumView = TablePayTotalItemView()
    private let serviceSumView = TablePayTotalItemView()
    private let separatorView = UIView()
    private let totalSumView = TablePayTotalItemView()
    private let closeButton = UIButton(type: .system)
    
    private let price: Double
    
    init(orderId: Int, price: Double) {
        viewModel = TablePayTotalViewModel(orderId: orderId)
        self.price = price
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        
        viewModel.showSuccessRelay.subscribe(onNext: { [unowned self] text in
            self.dismiss(animated: true) {
                self.delegate?.tablePayTotalViewControllerShowSuccess(text: text)
            }
        }).disposed(by: disposeBag)
        
        viewModel.showErrorRelay.subscribe(onNext: { [unowned self] error in
            self.dismiss(animated: true) {
                self.delegate?.tablePayTotalViewControllerShowError(error: error)
            }
        }).disposed(by: disposeBag)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        closeButton.setAllSideShadow(shadowShowSize: 1)
    }
}

extension TablePayTotalViewController {
    @objc private func closeButtonTapped() {
        closeButton.loadingIndicator(true)
        viewModel.bill()
    }
}

extension TablePayTotalViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(topSeparatorView)
        view.addSubview(verticalStackView)
        view.addSubview(closeButton)
        view.addSubview(separatorView)
        verticalStackView.addArrangedSubview(orderSumView)
        verticalStackView.addArrangedSubview(serviceSumView)
        verticalStackView.addArrangedSubview(SpacerView(space: 21))
        verticalStackView.addArrangedSubview(totalSumView)
        verticalStackView.addArrangedSubview(SpacerView(space: 16))
    }
    
    func setViewConstraints() {
        topSeparatorView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.centerX.equalToSuperview()
            make.width.equalTo(120)
            make.height.equalTo(3)
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.top.equalTo(topSeparatorView.snp.bottom).offset(40)
            make.leading.trailing.equalToSuperview()
        }
        
        separatorView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(serviceSumView.snp.bottom).offset(10)
            make.height.equalTo(1)
            make.width.equalTo(view.snp.width).offset(-48)
        }
        
        closeButton.snp.makeConstraints { make in
            make.top.equalTo(verticalStackView.snp.bottom)
            make.centerX.equalToSuperview()
            make.height.equalTo(52)
            make.width.equalTo(view.snp.width).offset(-48)
        }
    }
    
    func stylizeViews() {
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        topSeparatorView.layer.cornerRadius = 1.5
        topSeparatorView.backgroundColor = UIColor(rgb: 110, 129, 160, alpha: 23)
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fill
        verticalStackView.alignment = .fill
        verticalStackView.spacing = 0
        
        separatorView.backgroundColor = UIColor(rgb: 201, 215, 230, alpha: 70)
        
        orderSumView.set(description: price.description + " тг")
        
        serviceSumView.set(description: "0 тг")
        
        totalSumView.set(title: "Итого")
        totalSumView.set(titleFont: .systemFont(ofSize: 18, weight: .regular))
        totalSumView.set(titleColor: UIColor(hexString: "#334669"))
        totalSumView.set(description: price.description + " тг")
        totalSumView.set(descriptionFont: .systemFont(ofSize: 18, weight: .regular))
        totalSumView.set(descriptionColor: UIColor(hexString: "#FF647C"))
        
        closeButton.setTitle("Закрыть заказ", for: .normal)
        closeButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        closeButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)
    }
}
