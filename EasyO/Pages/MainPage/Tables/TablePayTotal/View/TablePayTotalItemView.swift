//
//  TablePayTotalItemView.swift
//  EasyO
//
//  Created by Аскар on 6/5/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TablePayTotalItemView: UIView {
    
    private let stackView = UIStackView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
    
    func set(description: String?) {
        descriptionLabel.text = description
    }
    
    func set(titleFont: UIFont) {
        titleLabel.font = titleFont
    }
    
    func set(titleColor: UIColor?) {
        titleLabel.textColor = titleColor
    }
    
    func set(descriptionFont: UIFont) {
        descriptionLabel.font = descriptionFont
    }
    
    func set(descriptionColor: UIColor?) {
        descriptionLabel.textColor = descriptionColor
    }
}

extension TablePayTotalItemView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(UIView())
        stackView.addArrangedSubview(descriptionLabel)
    }
    
    func setViewConstraints() {
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.bottom.lessThanOrEqualToSuperview().offset(-10)
        }
    }
    
    func stylizeViews() {
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.spacing = 4
        
        titleLabel.text = "Сумма заказа"
        titleLabel.font = .systemFont(ofSize: 16, weight: .regular)
        titleLabel.textColor = UIColor(rgb: 122, 138, 163, alpha: 70)
        
        descriptionLabel.text = "3500 тг"
        descriptionLabel.textColor = UIColor(hexString: "#334669")
        descriptionLabel.font = .systemFont(ofSize: 16, weight: .regular)
    }
}
