//
//  TablePayTotalViewModel.swift
//  EasyO
//
//  Created by Аскар on 6/7/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class TablePayTotalViewModel: BaseViewModel {
    
    let showSuccessRelay = PublishRelay<String>()
    let showErrorRelay = PublishRelay<AppError>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private let orderId: Int
    
    init(orderId: Int) {
        self.orderId = orderId
        super.init()
    }
    
    func bill() {
        guard let currentUser: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        let networkContext = BillsPostNetworkContext(
            amount: 1000,
            order: ObjectId(id: orderId),
            acceptedUser: ObjectId(id: currentUser.id)
        )
        UIApplication.shared.beginIgnoringInteractionEvents()
        networkService.loadDecodable(
            networkContext: networkContext,
            type: ObjectId.self
        ) { [weak self] result in
            UIApplication.shared.endIgnoringInteractionEvents()
            guard let strongSelf = self else { return }
            switch result {
            case .success:
                strongSelf.showSuccessRelay.accept("Оплата принята")
            case .error(let error):
                strongSelf.showErrorRelay.accept(error)
            }
        }
    }
}
