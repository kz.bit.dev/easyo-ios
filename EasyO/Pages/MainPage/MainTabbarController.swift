//
//  MainTabbarController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MainTabbarController: UITabBarController, BaseViewControllerProtocol {
    
    let activityIndicator = UIActivityIndicatorView()
    private let userType: UserType
    
    init(userType: UserType) {
        self.userType = userType
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch userType {
        case .waiter:
            setupWaiterControllers()
        case .cook:
            setupCookControllers()
        }
        
        tabBar.tintColor = .black
        tabBar.barTintColor = AppColor.skyBlue.uiColor
        tabBar.unselectedItemTintColor = UIColor(hexString: "#8FA0BD")
        
        setTabBarInset()
        
        showActivityIndicator()
        ClientSingleton.shared.loadData { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.hideActivityIndicator()
            switch result {
            case .success:
                print("Success")
            case .error(let error):
                print(error as Any)
            }
        }
    }
    
    private func setupCookControllers() {
        let iconSize = CGSize(width: 24, height: 24)
        
        let menuViewController = NavigationController(
            rootViewController: MenuMainViewController()
        )
        menuViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "menuTab")?.resized(to: iconSize),
            tag: 0
        )
        
        let tablesViewController = NavigationController(
            rootViewController: CookTablesViewController()
        )
        tablesViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "tablesTab")?.resized(to: iconSize),
            tag: 1
        )
        
        let profileViewController = NavigationController(
            rootViewController: ProfileViewController()
        )
        profileViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "profileTab")?.resized(to: iconSize),
            tag: 2
        )
        
        viewControllers = [
            menuViewController,
            tablesViewController,
            profileViewController
        ]
    }
    
    private func setupWaiterControllers() {
        let iconSize = CGSize(width: 24, height: 24)
        
        let menuViewController = NavigationController(
            rootViewController: MenuMainViewController()
        )
        menuViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "menuTab")?.resized(to: iconSize),
            tag: 0
        )
        
        let tablesViewController = NavigationController(
            rootViewController: TablesViewController()
        )
        tablesViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "tablesTab")?.resized(to: iconSize),
            tag: 1
        )
        
        // myOrdersTab
        let ordersViewController = NavigationController(rootViewController: OrderViewController())
        ordersViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "myOrdersTab")?.resized(to: iconSize),
            tag: 2
        )
        
        let profileViewController = NavigationController(
            rootViewController: ProfileViewController()
        )
        profileViewController.tabBarItem = UITabBarItem(
            title: nil,
            image: UIImage(named: "profileTab")?.resized(to: iconSize),
            tag: 3
        )
        
        viewControllers = [
            menuViewController,
            tablesViewController,
            ordersViewController,
            profileViewController
        ]
    }
    
    private func setTabBarInset() {
        guard let tabBarItems = tabBar.items else { return }
        for item in tabBarItems {
            item.imageInsets = UIEdgeInsets(top: 6, left: 0, bottom: -6, right: 0)
        }
    }
}
