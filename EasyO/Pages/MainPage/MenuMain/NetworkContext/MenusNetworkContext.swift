//
//  MenusNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class MenusNetworkContext: NetworkContext {
    
    let route: Route = .menus
    let method: NetworkMethod = .get
    var parameters = [String : Any]()
    let encoding: NetworkEncoding = .url
    
    init(page: Int = 0, size: Int = 1000) {
        parameters["page"] = page
        parameters["size"] = size
    }
}
