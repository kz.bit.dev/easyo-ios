//
//  MenuModel.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

struct MenuModel: Codable {
    let id: Int
    let menuType: String
    let fromDate: String?
    let toDate: String?
    let mealList: [MealModel]
    let imageUrl: String?
}
