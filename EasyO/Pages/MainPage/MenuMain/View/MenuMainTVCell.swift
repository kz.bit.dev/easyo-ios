//
//  MenuMainTVCell.swift
//  EasyO
//
//  Created by Аскар on 3/23/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuMainTVCell: UITableViewCell, ReusableView {
    
    private let menuImageView = CachedImageView()
    private let darkView = UIView()
    private let titleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        menuImageView.image = nil
    }
    
    func set(imageUrl: String?) {
        menuImageView.loadImage(urlString: imageUrl)
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
}

extension MenuMainTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(menuImageView)
        addSubview(darkView)
        addSubview(titleLabel)
    }
    
    func setViewConstraints() {
        menuImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo((Constants.width - 24) / 2)
            make.bottom.lessThanOrEqualToSuperview().offset(-8)
        }
        
        darkView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(menuImageView)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(menuImageView.snp.leading).offset(24)
            make.bottom.equalTo(menuImageView.snp.bottom).offset(-24)
            make.trailing.equalTo(menuImageView.snp.trailing).offset(-24)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        menuImageView.contentMode = .scaleAspectFill
        menuImageView.layer.cornerRadius = 20
        menuImageView.layer.masksToBounds = true
        
        darkView.layer.cornerRadius = menuImageView.layer.cornerRadius
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.32)
        
        titleLabel.text = "Супы"
        titleLabel.font = .boldSystemFont(ofSize: 24)
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .white
    }
}
