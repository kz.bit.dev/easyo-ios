//
//  MenuMainViewController.swift
//  EasyO
//
//  Created by Аскар on 3/23/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import UserNotifications

class MenuMainViewController: BaseRXTableViewController {
    
    private let viewModel = MenuMainViewModel()
    private var menus = [MenuModel]()
    
    private let socketService = SocketService()
    
    override func loadView() {
        super.loadView()
        baseViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        setupViewModel()
        
        if let token: String = LocalStore().getValue(forKey: .token) {
            socketService.openConnection(token: token)
        }
    }
    
    private func setupViewModel() {
        viewModel.menusRelay.subscribe(onNext: { [unowned self] menus in
            self.menus = menus
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadMenus()
    }
}

extension MenuMainViewController {
    @objc private func notificationsTapped() {
        guard let user: AuthUser = LocalStore().getValue(forKey: .currentUser) else { return }
        switch user.userPositionEnum {
        case .cook:
            let viewController = CookNotificationsViewController()
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        default:
            let viewController = NotificationsViewController()
            viewController.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension MenuMainViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menus.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuMainTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let menu = menus[indexPath.row]
        cell.set(title: menu.menuType)
        cell.set(imageUrl: menu.imageUrl)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let viewController = MenuMealsViewController(menu: menus[indexPath.row])
        viewController.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension MenuMainViewController {
    func stylizeViews() {
        navigationItem.title = "Меню"
        
        navigationController?.navigationBar.barTintColor = AppColor.skyBlue.uiColor
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .black
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: UIImage(named: "notificationIcon")?.resized(to: CGSize(width: 19.5, height: 24)),
            style: .done,
            target: self,
            action: #selector(notificationsTapped)
        )
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.contentInset.top = 8
        tableView.contentInset.bottom = 8
        tableView.separatorStyle = .none
        tableView.register(MenuMainTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
