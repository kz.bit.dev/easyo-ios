//
//  MenuMainViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class MenuMainViewModel: BaseViewModel {
    
    let menusRelay = PublishRelay<[MenuModel]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var menus = [MenuModel]()
    
    func loadMenus() {
        let networkContext = MenusNetworkContext()
        activityIndicatorStatePublishRelay.accept(.show)
        networkService.loadDecodable(
            networkContext: networkContext,
            type: PaginationData<MenuModel>.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
            switch result {
            case .success(let data):
                strongSelf.menus = data.data
                strongSelf.menusRelay.accept(data.data)
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
}
