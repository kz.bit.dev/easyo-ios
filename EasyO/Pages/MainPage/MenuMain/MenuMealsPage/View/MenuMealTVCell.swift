//
//  MenuMealTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/4/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuMealTVCell: UITableViewCell, ReusableView {
    
    private let mealImageView = CachedImageView()
    private let darkView = UIView()
    private let contentStackView = UIStackView()
    private let titleLabel = UILabel()
    private let priceLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        mealImageView.image = nil
        titleLabel.text = nil
        priceLabel.text = nil
    }
    
    func set(imageUrl: String?) {
        mealImageView.loadImage(urlString: imageUrl)
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
    
    func set(priceTitle: String?) {
        priceLabel.text = priceTitle
    }
}

extension MenuMealTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(mealImageView)
        addSubview(darkView)
        addSubview(contentStackView)
        contentStackView.addArrangedSubview(titleLabel)
        contentStackView.addArrangedSubview(priceLabel)
    }
    
    func setViewConstraints() {
        mealImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(8)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo((Constants.width - 24) / 3)
            make.bottom.lessThanOrEqualToSuperview().offset(-8)
        }
        
        darkView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(mealImageView)
        }
        
        contentStackView.snp.makeConstraints { make in
            make.leading.equalTo(mealImageView.snp.leading).offset(24)
            make.bottom.equalTo(mealImageView.snp.bottom).offset(-24)
            make.trailing.equalTo(mealImageView.snp.trailing).offset(-24)
        }
    }
    
    func stylizeViews() {
        backgroundColor = .clear
        selectionStyle = .none
        
        mealImageView.contentMode = .scaleAspectFill
        mealImageView.layer.cornerRadius = 20
        mealImageView.layer.masksToBounds = true
        
        darkView.backgroundColor = UIColor.black.withAlphaComponent(0.32)
        darkView.layer.cornerRadius = mealImageView.layer.cornerRadius
        
        contentStackView.axis = .vertical
        contentStackView.distribution = .fill
        contentStackView.alignment = .fill
        contentStackView.spacing = 4
        
        titleLabel.font = .boldSystemFont(ofSize: 24)
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .white
        
        priceLabel.font = .boldSystemFont(ofSize: 24)
        priceLabel.numberOfLines = 2
        priceLabel.textColor = .white
    }
}
