//
//  MenuMealsViewController.swift
//  EasyO
//
//  Created by Аскар on 5/4/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class MenuMealsViewController: BaseRXTableViewController {
    
    private let menu: MenuModel
    private let viewModel: MenuMealsViewModel
    private var meals = [MealModel]()
    
    init(menu: MenuModel) {
        self.menu = menu
        viewModel = MenuMealsViewModel(menu: menu)
        super.init(nibName: nil, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        setupViewModel()
    }
    
    private func setupViewModel() {
        viewModel.mealsRelay.subscribe(onNext: { [unowned self] meals in
            self.meals = meals
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadMeals()
    }
}

extension MenuMealsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return meals.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: MenuMealTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        let meal = meals[indexPath.row]
        cell.set(title: meal.name)
        cell.set(imageUrl: meal.imageUrl)
        cell.set(priceTitle: meal.getPriceText())
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension MenuMealsViewController {
    private func stylizeViews() {
        navigationItem.title = menu.menuType
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        tableView.separatorStyle = .none
        tableView.register(MenuMealTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
