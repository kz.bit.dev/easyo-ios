//
//  MenuMealsViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/2/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class MenuMealsViewModel: BaseViewModel {
    
    let mealsRelay = PublishRelay<[MealModel]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private let menu: MenuModel
    
    init(menu: MenuModel) {
        self.menu = menu
        super.init()
    }
    
    func loadMeals() {
        mealsRelay.accept(menu.mealList)
    }
}
