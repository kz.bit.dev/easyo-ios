//
//  NotificationsViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class NotificationsViewModel: BaseViewModel {
    
    let myComponentsRelay = PublishRelay<[NotificationComponent]>()
    let allComponentsRelay = PublishRelay<[NotificationComponent]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private let builder = NotificationsComponentBuilder()
    private var myComponents = [NotificationComponent]()
    private var allComponents = [NotificationComponent]()
    
    func loadComponents() {
        ClientSingleton.shared.orderLinesRelay.subscribe(onNext: { [unowned self] orderLines in
            self.setComponents(orderLines: orderLines)
        }).disposed(by: disposeBag)
        
        setComponents(orderLines: ClientSingleton.shared.orderLinesRelay.value)
    }
    
    func setComponents(orderLines: [OrderLineListItem]) {
        myComponents = builder.getMyComponentsFromOrders(orderLines: orderLines)
        allComponents = builder.getAllComponentsFromOrders(orderLines: orderLines)
        
        myComponentsRelay.accept(myComponents)
        allComponentsRelay.accept(allComponents)
    }
    
    func acceptTapped(type: NotificationOwnType, indexPath: IndexPath) {
        let component: NotificationComponent
        switch type {
        case .mine:
            component = myComponents[indexPath.row]
        case .another:
            component = allComponents[indexPath.row]
        }
        
        let dispatchGroup = DispatchGroup()
        activityIndicatorStatePublishRelay.accept(.show)
        for orderLine in component.orderLineList {
            let networkContext = getNetworkContext(orderLine: orderLine, orderId: component.orderId)
            dispatchGroup.enter()
            networkService.load(networkContext: networkContext) { response in
                dispatchGroup.leave()
                print(response.networkError as Any)
            }
        }
        dispatchGroup.notify(queue: .main) {
            self.activityIndicatorStatePublishRelay.accept(.hide)
            print("done")
        }
    }
    
    func getNetworkContext(orderLine: OrderLineListItem, orderId: Int) -> NetworkContext {
        return OrderLineUpdateNetworkContext(
            id: orderLine.id,
            orderLine: orderLine,
            status: .done,
            orderId: orderId
        )
    }
}
