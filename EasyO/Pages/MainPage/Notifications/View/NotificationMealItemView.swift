//
//  NotificationMealItemView.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class NotificationMealItemView: UIView, ReusableView {
    
    private let nameLabel = UILabel()
    private let quantityLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(title: String?) {
        nameLabel.text = title
    }
    
    func set(quantity: Int) {
        quantityLabel.text = quantity.description + " шт"
    }
}

extension NotificationMealItemView: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(nameLabel)
        addSubview(quantityLabel)
    }
    
    func setViewConstraints() {
        nameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalTo(quantityLabel.snp.leading).offset(-8)
        }
        
        quantityLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-24)
        }
    }
    
    func stylizeViews() {
        nameLabel.font = .systemFont(ofSize: 14, weight: .regular)
        nameLabel.textColor = UIColor(hexString: "#7A8AA3")
        nameLabel.text = "Плов"
        
        quantityLabel.font = .systemFont(ofSize: 14, weight: .regular)
        quantityLabel.textColor = UIColor(hexString: "#7A8AA3")
        quantityLabel.text = "4 шт"
        quantityLabel.textAlignment = .right
    }
}
