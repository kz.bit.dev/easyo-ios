//
//  TopTabBarCollectionViewCell.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class TopTabBarCollectionViewCell: UICollectionViewCell, ReusableView {
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                titleLabel.textColor = UIColor(hexString: "#33CCFF")
            } else {
                titleLabel.textColor = UIColor(hexString: "#999999").withAlphaComponent(0.4)
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                titleLabel.textColor = UIColor(hexString: "#33CCFF")
            } else {
                titleLabel.textColor = UIColor(hexString: "#999999").withAlphaComponent(0.4)
            }
        }
    }
    
    private let titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        setViewConstraints()
        stylizeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
    }
    
    private func setViewConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 4),
            titleLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -4)
        ]
        
        NSLayoutConstraint.activate(layoutConstraints)
    }
    
    private func stylizeViews() {
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        titleLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        titleLabel.textColor = UIColor(hexString: "#999999").withAlphaComponent(0.4)
    }
    
    func set(title: String) {
        titleLabel.text = title
    }
    
    func set(titleColor: UIColor) {
        titleLabel.textColor = titleColor
    }
}
