//
//  NotificationsHeaderView.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class NotificationsHeaderView: UIView {
    
    static let frame = CGRect(x: 0, y: 0, width: Constants.width, height: 72)
    
    // Input
    var controllerTitles = [String]() {
        didSet {
            stylize()
            addSubviews()
            setViewConstraints()
            createTabBarView()
        }
    }
    
    override var bounds: CGRect {
        didSet {
            createTabBarView()
        }
    }
    
    // Output
    weak var delegate: TopTabBarViewDelegate?
    
    // Private
    private let collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: TopTabBarCollectionViewFlowLayout()
    )
    private let bottomLineView = UIView()
    private let selectedIndicator = UIView()
    private let indicatorHeight: CGFloat = 5
    private var itemWidth: CGFloat = 0
    private var isFlexible: Bool = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func selectFirstItem() {
        guard !controllerTitles.isEmpty else { return }
        collectionView.selectItem(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .left)
    }
    
    private func stylize() {
        collectionView.register(cell: TopTabBarCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        
        bottomLineView.backgroundColor = UIColor(hexString: "#999999").withAlphaComponent(0.1)
        
        selectedIndicator.backgroundColor = UIColor(hexString: "#33CCFF")
    }
    
    private func addSubviews() {
        addSubview(collectionView)
        collectionView.addSubview(selectedIndicator)
        collectionView.addSubview(bottomLineView)
    }
    
    private func setViewConstraints() {
        var layoutConstraints = [NSLayoutConstraint]()
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        layoutConstraints += [
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24),
            collectionView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(layoutConstraints)
        
        bottomLineView.frame.origin.x = 0
        bottomLineView.frame.origin.y = frame.height - indicatorHeight
        bottomLineView.frame.size.height = indicatorHeight
        bottomLineView.frame.size.width = Constants.width - 48
        
        selectedIndicator.frame.origin.x = 0
        selectedIndicator.frame.origin.y = frame.height - indicatorHeight
        selectedIndicator.frame.size.height = indicatorHeight
    }
    
    private func createTabBarView() {
        guard frame.width > 0 && !controllerTitles.isEmpty else { return }
        
        let fullWidth = controllerTitles.map(calculateWidth).reduce(0, +)
        isFlexible = fullWidth > frame.width
        collectionView.isScrollEnabled = isFlexible
        itemWidth = (frame.width - 48) / CGFloat(controllerTitles.count)
        let calculatedWidthForFirstTitle = calculateWidth(forText: controllerTitles[0])
        let width = isFlexible ? calculatedWidthForFirstTitle : itemWidth
        selectedIndicator.frame.size.width = width
        
        collectionView.reloadData()
    }
    
    private func calculateWidth(forText text: String) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: 20)
        let boundingBox = text.boundingRect(
            with: constraintRect,
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .regular)],
            context: nil
        )
        return ceil(boundingBox.width + 24)
    }
}

extension NotificationsHeaderView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controllerTitles.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let cell: TopTabBarCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.set(title: controllerTitles[indexPath.row])
        return cell
    }
}

extension NotificationsHeaderView: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.topTabBarView(didSelectItemAt: indexPath)
        let cell: TopTabBarCollectionViewCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        UIView.animate(
            withDuration: 0.2,
            delay: 0,
            options: .curveEaseOut,
            animations: {
                self.selectedIndicator.frame.origin.x = cell.frame.origin.x
                let width = self.isFlexible ? cell.frame.width : self.itemWidth
                self.selectedIndicator.frame.size.width = width
            }
        )
    }
}

extension NotificationsHeaderView: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let calculatedWidthForTitle = calculateWidth(forText: controllerTitles[indexPath.row])
        let width = isFlexible ? calculatedWidthForTitle : itemWidth
        return CGSize(width: width, height: 44)
    }
}
