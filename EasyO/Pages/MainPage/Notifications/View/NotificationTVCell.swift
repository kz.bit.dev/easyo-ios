//
//  NotificationTVCell.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

protocol NotificationTVCellDelegate: class {
    func notificationTVCellAcceptTapped(indexPath: IndexPath)
}

class NotificationTVCell: UITableViewCell, ReusableView {
    
    weak var delegate: NotificationTVCellDelegate?
    var indexPath: IndexPath?
    
    private let stackView = UIStackView()
    private let titleLabel = UILabel()
    private let mealsStackView = UIStackView()
    private let acceptButton = UIButton(type: .system)
    private let topSeparatorView = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        titleLabel.text = nil
        for view in mealsStackView.arrangedSubviews {
            mealsStackView.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
    
    @objc private func acceptButtonTapped() {
        guard let indexPath = indexPath else { return }
        delegate?.notificationTVCellAcceptTapped(indexPath: indexPath)
    }
    
    func addShadowForButton() {
        acceptButton.setAllSideShadow(shadowShowSize: 1)
    }
    
    func set(component: NotificationComponent) {
        let optionalNumber = ClientSingleton.shared.getTableNumber(tableId: component.table.id)
        guard let number = optionalNumber else { return }
        titleLabel.text = "Готов заказ №\(number) столика"
        for orderLine in component.orderLineList {
            let view = NotificationMealItemView()
            guard let meal = ClientSingleton.shared.getMealById(id: orderLine.meal.id) else { continue }
            view.set(title: meal.name)
            view.set(quantity: orderLine.quantity)
            mealsStackView.addArrangedSubview(view)
        }
    }
}

extension NotificationTVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(topSeparatorView)
        addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(mealsStackView)
        stackView.addArrangedSubview(acceptButton)
    }
    
    func setViewConstraints() {
        topSeparatorView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
        
        stackView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(24)
            make.bottom.equalToSuperview().offset(-24)
            make.leading.trailing.equalToSuperview()
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
        }
        
        acceptButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo(52)
        }
    }
    
    func stylizeViews() {
        selectionStyle = .none
        backgroundColor = .clear
        
        topSeparatorView.backgroundColor = UIColor(hexString: "#C9D7E6").withAlphaComponent(0.7)
        
        stackView.distribution = .fill
        stackView.spacing = 20
        stackView.alignment = .fill
        stackView.axis = .vertical
        
        titleLabel.font = .systemFont(ofSize: 18, weight: .medium)
        titleLabel.text = "Готов заказ №1 столика"
        titleLabel.textColor = UIColor(hexString: "#334669")
        
        mealsStackView.axis = .vertical
        mealsStackView.spacing = 8
        mealsStackView.distribution = .fill
        mealsStackView.alignment = .fill
        
        acceptButton.setTitle("Принимаю", for: .normal)
        acceptButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        acceptButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        acceptButton.addTarget(self, action: #selector(acceptButtonTapped), for: .touchUpInside)
    }
}
