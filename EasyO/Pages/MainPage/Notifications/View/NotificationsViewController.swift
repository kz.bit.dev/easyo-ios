//
//  NotificationsViewController.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class NotificationsViewController: BaseRXTableViewController {
    
    private let viewModel = NotificationsViewModel()
    
    private let headerView = NotificationsHeaderView(frame: NotificationsHeaderView.frame)
    
    private var currentType = NotificationOwnType.mine
    private let headerTitleTypes = NotificationOwnType.allCases
    private var myComponents = [NotificationComponent]()
    private var allComponents = [NotificationComponent]()
    
    override func loadView() {
        super.loadView()
        baseViewModel = viewModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
        
        viewModel.myComponentsRelay.subscribe(onNext: { [unowned self] components in
            self.myComponents = components
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.allComponentsRelay.subscribe(onNext: { [unowned self] components in
            self.allComponents = components
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
        
        viewModel.loadComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        headerView.selectFirstItem()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
}

extension NotificationsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentType {
        case .mine:
            return myComponents.count
        case .another:
            return allComponents.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let component: NotificationComponent
        switch currentType {
        case .mine:
            component = myComponents[indexPath.row]
        case .another:
            component = allComponents[indexPath.row]
        }
        let cell: NotificationTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        cell.addShadowForButton()
        cell.indexPath = indexPath
        cell.delegate = self
        cell.set(component: component)
        return cell
    }
}

extension NotificationsViewController: NotificationTVCellDelegate {
    func notificationTVCellAcceptTapped(indexPath: IndexPath) {
        viewModel.acceptTapped(type: currentType, indexPath: indexPath)
    }
}

extension NotificationsViewController: TopTabBarViewDelegate {
    func topTabBarView(didSelectItemAt indexPath: IndexPath) {
        currentType = headerTitleTypes[indexPath.row]
        tableView.reloadData()
    }
}

extension NotificationsViewController {
    private func stylizeViews() {
        navigationItem.title = "Уведомления"
        
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        headerView.controllerTitles = headerTitleTypes.compactMap { $0.title }
        headerView.delegate = self
        
        tableView.separatorStyle = .none
        tableView.register(NotificationTVCell.self)
        tableView.estimatedRowHeight = 100
        tableView.tableHeaderView = headerView
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
    }
}
