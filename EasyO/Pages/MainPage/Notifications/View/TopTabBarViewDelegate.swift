//
//  TopTabBarViewDelegate.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

protocol TopTabBarViewDelegate: class {
    func topTabBarView(didSelectItemAt indexPath: IndexPath)
}
