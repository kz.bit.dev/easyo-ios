//
//  OrderLineUpdateNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 5/31/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

class OrderLineUpdateNetworkContext: NetworkContext {
    
    let route: Route
    let method: NetworkMethod = .put
    var parameters = [String: Any]()
    let encoding: NetworkEncoding = .json
    
    init(id: Int, orderLine: OrderLineListItem, status: OrderLineListItemStatusType, orderId: Int) {
        route = .orderLineById(id: id)
        
        parameters = orderLine.getParameters()
        parameters["status"] = status.rawValue
        parameters["order"] = ObjectId(id: orderId).getParameters()
        print(parameters.jsonStringRepresentation as Any)
    }
    
    init(
        id: Int,
        quantity: Int,
        unitPrice: Double,
        meal: ObjectId,
        order: ObjectId,
        acceptedUser: ObjectId,
        status: OrderLineListItemStatusType
    ) {
        route = .orderLineById(id: id)
        
        parameters["quantity"] = quantity
        parameters["unitPrice"] = unitPrice
        parameters["meal"] = meal.getParameters()
        parameters["order"] = order.getParameters()
        parameters["acceptedUser"] = acceptedUser.getParameters()
        parameters["status"] = status.rawValue
    }
}
