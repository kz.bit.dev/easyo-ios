//
//  NotificationComponent.swift
//  EasyO
//
//  Created by Аскар on 5/30/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class NotificationComponent {
    
    let acceptedUser: ObjectId
    let table: ObjectId
    let orderLineList: [OrderLineListItem]
    let orderId: Int
    
    init(acceptedUser: ObjectId, table: ObjectId, orderLineList: [OrderLineListItem], orderId: Int) {
        self.acceptedUser = acceptedUser
        self.table = table
        self.orderLineList = orderLineList
        self.orderId = orderId
    }
}
