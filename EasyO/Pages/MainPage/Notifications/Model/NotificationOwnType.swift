//
//  NotificationOwnType.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

enum NotificationOwnType: CaseIterable {
    
    case mine
    case another
    
    var title: String {
        switch self {
        case .mine:
            return "Мои заказы"
        case .another:
            return "Все заказы"
        }
    }
}
