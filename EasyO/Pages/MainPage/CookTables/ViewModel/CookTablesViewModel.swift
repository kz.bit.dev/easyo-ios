//
//  CookTablesViewModel.swift
//  EasyO
//
//  Created by Аскар on 5/31/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class CookTablesViewModel: BaseViewModel {
    
    let myTablesRelay = PublishRelay<[TableModel]>()
    let ordersRelay = PublishRelay<[OrderModel]>()
    
    private let networkService: NetworkService = NetworkAdapter()
    private var tables = [TableModel]()
    
    override init() {
        super.init()
        subscribeClient()
        tables = ClientSingleton.shared.tablesRelay.value
    }
    
    func loadTables() {
        myTablesRelay.accept(tables)
    }

    func subscribeClient() {
        ClientSingleton.shared.tablesRelay.subscribe(onNext: { [unowned self] tables in
            self.tables = tables
            self.myTablesRelay.accept(tables)
        }).disposed(by: disposeBag)
    }
}
