//
//  LoginViewController.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit
import SnapKit
import InputMask

class LoginViewController: BaseRXViewController {
    
    private let viewModel = LoginViewModel()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private let backgroundView = UIView()
    private let scrollView = ScrollView()
    private let iconImageView = UIImageView()
    private let cardView = UIView()
    private let verticalStackView = UIStackView()
    private let emailContainerView = UIStackView()
    private let emailTitle = UILabel()
    private let emailTextField = MaskedRoundedTextField()
    private let passwordContainerView = UIStackView()
    private let passwordTitle = UILabel()
    private let passwordTextField = MaskedRoundedTextField()
    private let loginButton = UIButton(type: .system)
    
    private var keyboardObservers = [Any]()
    private var token: String?
    private let userType: UserType
    
    init(userType: UserType) {
        self.userType = userType
        super.init(nibName: nil, bundle: nil)
        baseViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        set(keyboardObservers: &keyboardObservers)
        loginButton.setAllSideShadow(shadowShowSize: 1)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        remove(keyboardObservers: &keyboardObservers)
    }
}

extension LoginViewController {
    @objc private func loginButtonTapped() {
        viewModel.login(email: emailTextField.text, password: passwordTextField.text)
    }
    
    @objc private func backgroundTapped() {
        view.endEditing(true)
    }
}

extension LoginViewController {
    private func setupViewModel() {
        viewModel.loginShakeRelay.subscribe(onNext: { [unowned self] in
            self.emailContainerView.shakeView()
        }).disposed(by: disposeBag)
        
        viewModel.passwordShakeRelay.subscribe(onNext: { [unowned self] in
            self.passwordContainerView.shakeView()
        }).disposed(by: disposeBag)
        
        viewModel.loginRelay.subscribe(onNext: { [unowned self] in
            self.login()
        }).disposed(by: disposeBag)
    }
    
    private func login() {
        let viewController = MainTabbarController(userType: userType)
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true)
    }
}

extension LoginViewController: KeyboardCoverProtocol {
    var targetFrame: CGRect? {
        let frame = emailTextField.frame.applying(CGAffineTransform(translationX: 0, y: 32))
        return cardView.convert(frame, to: scrollView.contentView)
    }
    
    var scrollableView: UIScrollView { return scrollView }
}

extension LoginViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(backgroundView)
        view.addSubview(scrollView)
        scrollView.contentView.addSubview(iconImageView)
        scrollView.contentView.addSubview(cardView)
        cardView.addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(emailContainerView)
        verticalStackView.addArrangedSubview(UIView())
        verticalStackView.addArrangedSubview(passwordContainerView)
        verticalStackView.addArrangedSubview(UIView())
        verticalStackView.addArrangedSubview(UIView())
        verticalStackView.addArrangedSubview(UIView())
        verticalStackView.addArrangedSubview(loginButton)
        emailContainerView.addArrangedSubview(emailTitle)
        emailContainerView.addArrangedSubview(emailTextField)
        passwordContainerView.addArrangedSubview(passwordTitle)
        passwordContainerView.addArrangedSubview(passwordTextField)
    }
    
    func setViewConstraints() {
        backgroundView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalToSuperview()
        }
        
        scrollView.contentView.snp.makeConstraints { make in
            make.centerY.centerX.equalToSuperview()
            make.width.equalTo(scrollView.snp.width)
            make.height.equalTo(scrollView.snp.height)
        }
        
        iconImageView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(cardView.snp.top).offset(-40)
            make.width.height.equalTo(45)
        }
        
        cardView.snp.makeConstraints { make in
            make.leading.equalTo(scrollView.contentView.snp.leading).offset(16)
            make.trailing.equalTo(scrollView.contentView.snp.trailing).offset(-16)
            make.centerY.equalTo(scrollView.contentView.snp.centerY).offset(-32)
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.leading.equalTo(cardView.snp.leading).offset(16)
            make.trailing.equalTo(cardView.snp.trailing).offset(-16)
            make.top.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
        }
        
        emailTextField.snp.makeConstraints { make in
            make.height.equalTo(52)
        }
        
        emailContainerView.snp.makeConstraints { make in
            make.width.equalTo(view.snp.width).offset(-64)
        }
        
        passwordContainerView.snp.makeConstraints { make in
            make.width.equalTo(view.snp.width).offset(-64)
        }
        
        passwordTextField.snp.makeConstraints { make in
            make.height.equalTo(52)
        }
        
        loginButton.snp.makeConstraints { make in
            make.height.equalTo(52)
            make.width.equalTo(view.snp.width).offset(-64)
        }
    }
    
    func stylizeViews() {
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        setDefaultNavigationBar()
        navigationItem.title = "Вход"
        
        scrollView.keyboardDismissMode = .onDrag
        scrollView.contentSize = scrollView.contentView.frame.size
        scrollView.delaysContentTouches = false
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(backgroundTapped)))
        
        iconImageView.image = UIImage(named: "appIcon")
        iconImageView.contentMode = .scaleAspectFill
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fill
        verticalStackView.alignment = .center
        verticalStackView.spacing = 12
        
        emailTitle.text = "Электронная почта"
        emailTitle.textColor = UIColor(hexString: "#334669")
        emailTitle.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        emailContainerView.axis = .vertical
        emailContainerView.distribution = .fill
        emailContainerView.alignment = .fill
        emailContainerView.spacing = 12
        
        emailTextField.textfield.tintColor = .black
        emailTextField.textfield.font = .systemFont(ofSize: 17)
        emailTextField.textfield.textColor = .black
        emailTextField.textfield.keyboardType = .emailAddress
        emailTextField.textfield.attributedPlaceholder = NSAttributedString(
            string: "example@gmail.com",
            attributes: [.foregroundColor: UIColor(hexString: "#334669").withAlphaComponent(0.3)]
        )
        
        passwordTitle.text = "Пароль"
        passwordTitle.textColor = UIColor(hexString: "#334669")
        passwordTitle.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        passwordContainerView.axis = .vertical
        passwordContainerView.distribution = .fill
        passwordContainerView.alignment = .fill
        passwordContainerView.spacing = 12
        
        passwordTextField.textfield.tintColor = .black
        passwordTextField.textfield.isSecureTextEntry = true
        passwordTextField.textfield.textAlignment = .left
        passwordTextField.textfield.font = .systemFont(ofSize: 17)
        passwordTextField.textfield.textColor = .black
        passwordTextField.textfield.layer.borderColor = UIColor.clear.cgColor
        passwordTextField.textfield.layer.borderWidth = 0
        passwordTextField.textfield.keyboardType = .default
        passwordTextField.textfield.attributedPlaceholder = NSAttributedString(
            string: "password",
            attributes: [
                .foregroundColor: UIColor(hexString: "#334669").withAlphaComponent(0.3)
            ]
        )
        
        loginButton.setTitle("Войти", for: .normal)
        loginButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        loginButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        loginButton.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
    }
    
    private func setDefaultNavigationBar() {
        guard let navigationBar = navigationController?.navigationBar else {
            return
        }
        navigationBar.barStyle = .black
        navigationBar.shadowImage = UIImage()
        navigationBar.tintColor = UIColor.black
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black]
        navigationBar.setBackgroundImage(UIImage(), for: .default)
    }
}
