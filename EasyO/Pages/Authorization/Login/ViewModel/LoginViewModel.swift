//
//  LoginViewModel.swift
//  EasyO
//
//  Created by Аскар on 3/22/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import RxCocoa

class LoginViewModel: BaseViewModel {
    
    let loginShakeRelay = PublishRelay<Void>()
    let passwordShakeRelay = PublishRelay<Void>()
    let loginRelay = PublishRelay<Void>()
    
    private let networkService: NetworkService = NetworkAdapter()
    
    func login(email: String, password: String) {
        guard !email.isEmpty else {
            loginShakeRelay.accept(())
            return
        }
        guard !password.isEmpty else {
            passwordShakeRelay.accept(())
            return
        }
        activityIndicatorStatePublishRelay.accept(.show)
        let networkContext = SessionsAuthNetworkContext(login: email, password: password)
        networkService.loadDecodable(
            networkContext: networkContext,
            type: AuthModel.self
        ) { [weak self] result in
            guard let strongSelf = self else { return }
            strongSelf.activityIndicatorStatePublishRelay.accept(.hide)
            switch result {
            case .success(let model):
                let adapter = AppRequestAdapter()
                adapter.token = model.token
                sessionManager.set(adapter: adapter)
                strongSelf.loadFcm { [weak self] result in
                    guard let strongSelf = self else { return }
                    switch result {
                    case .success:
                        LocalStore().set(value: model.token, forKey: .token)
                        LocalStore().set(value: model.user, forKey: .currentUser)
                        strongSelf.loginRelay.accept(())
                    case .error(let error):
                        strongSelf.errorDisplayPublishRelay.accept(error)
                    }
                }
            case .error(let error):
                strongSelf.errorDisplayPublishRelay.accept(error)
            }
        }
    }
    
    private func loadFcm(completion: @escaping (Result<Void>) -> Void) {
        let networkContext = SessionsFcmNetworkContext()
        networkService.loadDecodable(networkContext: networkContext, type: ObjectId.self) { result in
            switch result {
            case .success:
                completion(.success(()))
            case .error(let error):
                completion(.error(error))
            }
        }
    }
}
