//
//  SessionsFcmNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 6/6/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Firebase

class SessionsFcmNetworkContext: NetworkContext {
    
    let route: Route
    let method: NetworkMethod = .put
    var parameters = [String: Any]()
    var encoding: NetworkEncoding = .url
    
    init() {
        let fcmToken = Messaging.messaging().fcmToken
        route = .sessionsFcm(fcm: fcmToken ?? "lol")
    }
}
