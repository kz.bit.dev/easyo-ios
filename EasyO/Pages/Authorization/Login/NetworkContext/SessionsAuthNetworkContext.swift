//
//  SessionsAuthNetworkContext.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

class SessionsAuthNetworkContext: NetworkContext {
    
    let route: Route = .sessionsAuth
    let method: NetworkMethod = .post
    var parameters = [String: Any]()
    var encoding: NetworkEncoding = .json
    
    init(login: String, password: String) {
        parameters["login"] = login
        parameters["password"] = password
    }
}
