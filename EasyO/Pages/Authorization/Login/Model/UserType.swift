//
//  UserType.swift
//  EasyO
//
//  Created by Аскар on 5/12/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

enum UserType {
    case cook
    case waiter
}
