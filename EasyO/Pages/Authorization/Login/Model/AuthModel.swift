//
//  AuthModel.swift
//  EasyO
//
//  Created by Аскар on 4/27/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import Foundation

struct AuthModel: Codable {
    let token: String
    let user: AuthUser
}

struct AuthUser: Codable, LosslessStringConvertible, CustomStringConvertible {
    let id: Int
    let imageUrl: String?
    let firstName: String
    let lastName: String
    let email: String?
    let userPositionEnum: UserPositionEnum?
    
    init?(_ description: String) {
        guard let data = description.data(using: .utf8) else { return nil }
        guard let item = try? JSONDecoder().decode(AuthUser.self, from: data) else {
            return nil
        }
        self = item
    }

    var description: String {
        guard let jsonData = try? JSONEncoder().encode(self) else { return "" }
        let jsonString = String(data: jsonData, encoding: .utf8)
        return jsonString!
    }
}

enum UserPositionEnum: String, Codable {
    
    case waiter = "WAITER"
    case cook = "COOK"
    case manager = "MANAGER"
    
    var title: String {
        switch self {
        case .waiter:
            return "Официант"
        case .cook:
            return "Повор"
        case .manager:
            return "Менеджер"
        }
    }
}
