//
//  OnboardingComponent.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

struct OnboardingComponent {
    let image: UIImage?
    let title: String
    let description: String
}
