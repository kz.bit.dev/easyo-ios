//
//  OnboardingViewController.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    private let layout = HorizontalCVLayout()
    private lazy var collectionView = UICollectionView(
        frame: .zero,
        collectionViewLayout: layout
    )
    private let pageControl = UIPageControl()
    private let loginAsWaiterButton = UIButton(type: .system)
    private let loginAsCookButton = UIButton(type: .system)
    
    let components: [OnboardingComponent] = [
        .init(
            image: UIImage(named: "onboarding1"),
            title: "Заказы",
            description: "Легко принимайте заказы и \nприступайте к приготовлению"
        ),
        .init(
            image: UIImage(named: "onboarding2"),
            title: "Меню",
            description: "В разделе меню вы найдете все \nвиды блюд этого ресторана"
        ),
        .init(
            image: UIImage(named: "onboarding3"),
            title: "Профиль",
            description: "Вы можете посмотреть и изменить \nваши данные в разделе “профиль”"
        )
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loginAsWaiterButton.setAllSideShadow(shadowShowSize: 1)
    }
    
    @objc private func loginAsWaiterButtonTapped() {
        LocalStore().set(value: true, forKey: .onboardingShowed)
        let viewController = LoginViewController(userType: .waiter)
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true)
    }
    
    @objc private func loginAsCookButtonTapped() {
        LocalStore().set(value: true, forKey: .onboardingShowed)
        let viewController = LoginViewController(userType: .cook)
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: true)
    }
}

extension OnboardingViewController: UICollectionViewDataSource {
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        return components.count
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        let cell: OnboardingCVCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        let component = components[indexPath.row]
        cell.set(image: component.image)
        cell.set(title: component.title)
        cell.set(description: component.description)
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
    }
}
 
extension OnboardingViewController: UICollectionViewDelegate {}

extension OnboardingViewController: ViewInstallationProtocol {
    func addSubviews() {
        view.addSubview(collectionView)
        view.addSubview(pageControl)
        view.addSubview(loginAsWaiterButton)
        view.addSubview(loginAsCookButton)
    }
    
    func setViewConstraints() {
        collectionView.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
        }
        
        pageControl.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(OnboardingCVCell.height)
            make.centerX.equalToSuperview()
        }
        
        loginAsWaiterButton.snp.makeConstraints { make in
            make.top.equalTo(pageControl.snp.bottom).offset(32)
            make.leading.equalToSuperview().offset(24)
            make.trailing.equalToSuperview().offset(-24)
            make.height.equalTo(52)
        }
        
        loginAsCookButton.snp.makeConstraints { make in
            make.top.equalTo(loginAsWaiterButton.snp.bottom).offset(24)
            make.leading.trailing.equalTo(loginAsWaiterButton)
        }
    }
    
    func stylizeViews() {
        view.backgroundColor = AppColor.skyBlue.uiColor
        
        layout.itemSize = CGSize(width: Constants.width, height: Constants.heigth)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        collectionView.contentInset.top = 16
        collectionView.contentInset.bottom = 16
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(cell: OnboardingCVCell.self)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isPagingEnabled = true
        
        pageControl.numberOfPages = components.count
        pageControl.currentPageIndicatorTintColor = UIColor(hexString: "#33CCFF")
        pageControl.pageIndicatorTintColor = UIColor(hexString: "#CAD7E7")
        
        loginAsWaiterButton.setTitle("Войти как официант", for: .normal)
        loginAsWaiterButton.setTitleColor(UIColor(hexString: "#2E476E"), for: .normal)
        loginAsWaiterButton.backgroundColor = UIColor(hexString: "#E8F3FB")
        loginAsWaiterButton.addTarget(self, action: #selector(loginAsWaiterButtonTapped), for: .touchUpInside)
        
        loginAsCookButton.setTitle("Войти как повар", for: .normal)
        loginAsCookButton.setTitleColor(UIColor(hexString: "#00BFFF"), for: .normal)
        loginAsCookButton.addTarget(self, action: #selector(loginAsCookButtonTapped), for: .touchUpInside)
    }
}
