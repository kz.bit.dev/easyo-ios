//
//  OnboardingCVCell.swift
//  EasyO
//
//  Created by Аскар on 5/11/20.
//  Copyright © 2020 askar.ulubayev. All rights reserved.
//

import UIKit

class OnboardingCVCell: UICollectionViewCell, ReusableView {
    
    static let height = 128 + 150 + 56 + 76 + 40
    
    private let imageView = UIImageView()
    private let verticalStackView = UIStackView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(image: UIImage?) {
        imageView.image = image
    }
    
    func set(title: String?) {
        titleLabel.text = title
    }
    
    func set(description: String?) {
        descriptionLabel.text = description
    }
}

extension OnboardingCVCell: ViewInstallationProtocol {
    func addSubviews() {
        addSubview(imageView)
        addSubview(verticalStackView)
        verticalStackView.addArrangedSubview(titleLabel)
        verticalStackView.addArrangedSubview(descriptionLabel)
    }
    
    func setViewConstraints() {
        imageView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(128)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(150)
        }
        
        verticalStackView.snp.makeConstraints { make in
            make.top.equalTo(imageView.snp.bottom).offset(56)
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
        }
    }
    
    func stylizeViews() {
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "onboarding1")
        
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .fill
        verticalStackView.spacing = 16
        verticalStackView.alignment = .fill
        
        titleLabel.textAlignment = .center
        titleLabel.font = .systemFont(ofSize: 30, weight: .medium)
        titleLabel.text = "Заказы"
        
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = .systemFont(ofSize: 14, weight: .regular)
        descriptionLabel.text = "Легко принимайте заказы и \nприступайте к приготовлению"
        descriptionLabel.numberOfLines = 0
    }
}
